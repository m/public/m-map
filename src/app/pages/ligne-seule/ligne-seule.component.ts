import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClustersService, LinesService, MapFilter } from '@metromobilite/m-features/core';
import { MapFiltersService, MLayersLinesSource, MLayersPoiLayer, MLayersPoiSource } from '@metromobilite/m-layers';
import { GeolocationSource, LayerInputConfig, MapComponent, MapLayersService } from '@metromobilite/m-map';
import { Feature, MapBrowserEvent } from 'ol';
import BaseEvent from 'ol/events/Event';
import { transform } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import { Observable, Subject } from 'rxjs';
import { publish, shareReplay } from 'rxjs/operators';
import { MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { FeatureLike } from 'ol/Feature';
import { DataDialComponent } from 'src/app/components/data-dial/data-dial.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-ligne-seule',
  templateUrl: './ligne-seule.component.html',
  styleUrls: ['./ligne-seule.component.scss']
})
export class LigneSeuleComponent implements OnInit, OnDestroy {

  zoom = 13;
  tracking = false;

  tiles: LayerInputConfig[] = [
    { layer: 'core:xyz-light', visible: false },
    { layer: 'core:xyz-dark', visible: true },
    { layer: 'core:dynamic', visible: false },
  ];

  layers: LayerInputConfig[] = [];
  leafs: MapFilter[] = [];
  lineID = 'SEM:B';
  ligne
  clusterIds: string[] = [];
  clusters = []

  @ViewChild(MapComponent, { static: true }) map!: MapComponent;

  private geoloactionConfig: LayerInputConfig;
  private unsubscriber = new Subject();
  public selectedFeature: any;

  constructor (
    private geolocationSource: GeolocationSource,
		private route: ActivatedRoute,
    private mapFiltersService: MapFiltersService,
    private linesService: LinesService,
		private linesSource: MLayersLinesSource,
		private poiSource: MLayersPoiSource,
		public dialog: MatDialog,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string
  ) {
    this.geoloactionConfig = { layer: 'core:geolocation', visible: true };
		this.lineID = this.route.snapshot.params.id;
    this.route.snapshot.data.clusters.map((cluster:any)=>{
      this.clusters.push(cluster)
      this.clusterIds.push(`clusters_${cluster.code}`);
    })
    this.ligne = this.linesService.find(this.lineID);

  }

  ngOnInit(): void {
    this.mapFiltersService.init(this.map).subscribe(result => {
      this.leafs = this.mapFiltersService.leafs;
      if (result && this.mapFiltersService.initialized) {
        this.layers = [this.geoloactionConfig, ...this.mapFiltersService.layers];
        // Make sure to wait for layers.
        this.geolocationSource.hideAccuracyFeature();
        const lineType = 'line:' + this.ligne.type;
        const appStateConfig = `line:${this.ligne.type}:${this.lineID}`;
        const appStateConfig1 = `line:${this.ligne.type}:SEM:A`;
        const appStateConfig2 = `line:${this.ligne.type}:SEM:B`;
        const appStateConfig3 = `line:${this.ligne.type}:SEM:D`;
        const appStateConfig4 = `line:SCOL:TPV:BE11`;
        // const layerLine = this.mapFiltersService.getLayersFromState(lineType)[0];
        const layerClusters = this.mapFiltersService.getLayersFromState('poi:clusters')[0]
        layerClusters.ids = [...this.clusterIds];

        this.mapFiltersService.setState([
          appStateConfig, appStateConfig1, appStateConfig2, appStateConfig3, appStateConfig4,
          'poi:clusters'
        ]).subscribe((update) => {
          const lineFeature = (this.linesSource.getSource() as VectorSource).getFeatureById(this.lineID);
          if (lineFeature) {
            // normalement faut coder une fonction pour pas avoir à faire ça 10 fois, mais bon, là on est seulement sur un outil de test de feature donc balek
            const extent = lineFeature.getGeometry().getExtent();
            if (extent) {
              const padding = 32;
              const fit$ = new Observable(obs => {
                this.map.view.fit(extent, {
                  padding: [padding + 16, padding, padding + window.innerHeight * 0.55, padding],
                  duration:1500,
                  callback: complete => {
                    obs.next(complete);
                    obs.complete();
                  }
                });
              }).pipe(shareReplay(1));
              publish()(fit$).connect();
            }
          }
          //une fois que les poiSources sont généré, on rend visible ceux qui nous intéressent
          this.poiSource.isComplete().then(()=>{
            (this.poiSource.getSource() as VectorSource).getFeatures().filter(f=>{
              return f.get('code') && this.clusters.find(c=> c.code === f.get('code'))
            }).forEach(c => {
              c.set('visible',true)
            });
          })
        })
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  onMapClick(event: BaseEvent): void {
    if (this.selectedFeature) {
      this.selectedFeature.set('selected', false);
      this.selectedFeature = undefined;
    }
    if (event instanceof MapBrowserEvent) {
      event.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        if (!this.selectedFeature) {
          (feature as Feature).set('selected', true);
          this.selectedFeature = feature;
          this.openDataDialog(feature)
          //wait for angular to open element
          setTimeout(()=>{
            document.querySelector('.mat-expanded').scrollIntoView()

            //si possible, centre la map sur le point.
            if(typeof feature.getGeometry()['getCoordinates'] === 'function'){
              this.map.view.setCenter(feature.getGeometry()['getCoordinates']())
            }
          })
        }
      });
    }
  }

  onPanelClick(_,arret){
    // on s'assure que l'arrêt est fait avant de chercher à centrer dessus.
    if(arret.lat && arret.lon){
      this.map.view.setCenter(transform([arret.lon, arret.lat], this.dataProjection, this.featureProjection))
    }
  }

  openDataDialog(feature : FeatureLike): void {
		this.dialog.open(DataDialComponent, {
      data:feature.getProperties(),
			panelClass: ['m-theme', 'full-screen-mobile']
		});
	}

}
