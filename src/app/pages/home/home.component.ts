import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NavigationStart, Router } from '@angular/router';
import {
  ClustersService,
  LetaxiService,
  LinesService,
  MapFilter,
} from '@metromobilite/m-features/core';
import {
  MapFiltersService,
  MLayersCitizyeaSource,
  MLayersDisturbancesSource,
  MLayersLetaxiSource,
  MLayersLinesSource,
  MLayersPoiSource,
  MLayersRoadSectionSource,
  PoiLayerHelper,
} from '@metromobilite/m-layers';
import {
  GeolocationSource,
  LayerInputConfig,
  MAP_PROJECTION_DESTINATION,
  MAP_PROJECTION_SOURCE,
  MapComponent,
  MapLayersService,
  XYZDynamicSource,
} from '@metromobilite/m-map';
import { Feature, MapBrowserEvent } from 'ol';
import { Coordinate } from 'ol/coordinate';
import BaseEvent from 'ol/events/Event';
import { Extent } from 'ol/extent';
import { FeatureLike } from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import { merge, Observable, of, Subject } from 'rxjs';
import { delay, filter, publish, shareReplay, take, takeUntil } from 'rxjs/operators';
import { DataDialComponent } from 'src/app/components/data-dial/data-dial.component';
import { LetaxiMapService } from '../../../../projects/m-map/m-layers/src/services/letaxiMap.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit, OnDestroy {
  zoom = 10;
  tracking = false;

  tiles: LayerInputConfig[] = [
    { layer: 'core:xyz-light', visible: true },
    { layer: 'core:xyz-dark', visible: false },
    { layer: 'core:dynamic', visible: false },
  ];

  layers: LayerInputConfig[] = [];
  leafs: MapFilter[] = [];
  lineID = 'SEM:A';
  clusterIds: string[] = [];
  coordinate: number[] = [];

  @ViewChild(MapComponent, { static: true }) map!: MapComponent;
  public formCustomAdresse: FormGroup = new FormGroup({
    lat: new FormControl('45.1810', [Validators.required]),
    lon: new FormControl('5.7030', [Validators.required]),
  });
  private geolocationConfig: LayerInputConfig;
  private taxiConfig: LayerInputConfig;
  private unsubscriber = new Subject();
  private selectedFeature: any;

  public centreCarte : number[];
	public centreCarteFinished = new Subject<Boolean>();
  winterServiceConfig: LayerInputConfig;

  indexFondDeCarte = 0

  constructor (
    private poiSource: MLayersPoiSource,
    private linesSource: MLayersLinesSource,
    private roadSectionSource: MLayersRoadSectionSource,
    private geolocationSource: GeolocationSource,
    private citizyeaSource: MLayersCitizyeaSource,
    private layersService: MapLayersService,
    private mapFiltersService: MapFiltersService,
    private poiLayerHelper: PoiLayerHelper,
    private linesService: LinesService,
    private clusterService: ClustersService,
    public dialog: MatDialog,
    private router: Router,
    private letaxiSource: MLayersLetaxiSource,
    private leTaxiService: LetaxiMapService,
    private letaxidynService: LetaxiService,
    private xYZDynamicSource: XYZDynamicSource,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private disturbancesSource: MLayersDisturbancesSource
  ) {

    this.geolocationConfig = { layer: 'core:geolocation', visible: true };
    this.taxiConfig = { layer: 'm-layers:letaxi', visible: false };
			this.router.events.subscribe((value: any) => {
      const url = new URL(value.url, window.location.origin);
      const params = new URLSearchParams(url.search);
		})

    this.centreCarte = [5.589119, 45.367240];

  }

  ngAfterViewInit(): void {
      const appStateConfig1 = `line:TRAM:SEM:A`;
      const appStateConfig3 = `line:TRAM:SEM:D`;
      const appStateConfig4 = `line:SCOL:TPV:BE11`;

      this.clusterService
        .getClusters(this.lineID)
        .subscribe((clustersResponse: any[]) => {
          clustersResponse.forEach((clusterResponse) => {
            this.clusterIds.push(`clusters_${clusterResponse.code}`);
          });
        });
      this.mapFiltersService.init(this.map).subscribe((result) => {
        this.leafs = this.mapFiltersService.leafs.sort((a, b) =>
          a.name.localeCompare(b.name)
        );
        if (result && this.mapFiltersService.initialized) {
          this.layers = [
            this.taxiConfig,
            this.geolocationConfig,
            ...this.mapFiltersService.layers,
          ];
          // console.log({layers: this.layers})
          // Make sure to wait for layers.
          setTimeout(() => {
            this.geolocationSource.hideAccuracyFeature();
            // const layer = this.mapFiltersService.getLayersFromState('line:TRAM')[0];
            // layer.ids = ['SEM:E', 'SEM:B', 'SEM:C'];
            this.setState([
              // appStateConfig1, appStateConfig3, appStateConfig4, 
              // 'disturbances', 
              // 'road-section',
              // 'poi:clusters',
              // 'poi:free-scooter',
              // 'poi:free-floating-bike:dott',
              // 'poi:free-floating-scooter:dott',
              // 'app:letaxi',
              // 'poi:letaxi',
            ]);
          });
        }
      });
      // this.enableTracking();
      this.mapFiltersService.updates$
        .pipe(takeUntil(this.unsubscriber))
        .subscribe(() => {
          console.log('update !');
        });
      this.geolocationSource.position$
        .pipe(takeUntil(this.unsubscriber))
        .subscribe((position) => {
          console.log('position', position);
          this.coordinate = position;
        });
      this.geolocationSource.enable$
        .pipe(takeUntil(this.unsubscriber))
        .subscribe((enabled) => {
          console.log('geolocation', enabled);
        });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  log(data: any): void {
    console.log(data);
  }

  toggleTiles(): void {
    this.indexFondDeCarte++;
    if(this.indexFondDeCarte > this.tiles.length -1) this.indexFondDeCarte = 0;''
    // If you want to add/remove layers or update their visibility,
    // You must create a whole new list of LayerInputConfig objects.
    // This way, you trigger the onChanges lifecycle hook of the m-map component.
    // Changes the whole list of LayerInputConfig doesn't necessarily mean that the
    // already created layers will be recreated.
    this.tiles = JSON.parse(JSON.stringify(this.tiles)).map(
      (config: LayerInputConfig, index : number) => {
        config.visible = index == this.indexFondDeCarte;
        return config;
      }
    );
    const useDark = this.tiles
      .filter((config) => config.visible)
      .reduce((_, config) => config.layer.includes('dark'), true);
    // If you don't need to change the visibility of the layers,
    // you can just update the config objects and call the changed() method on each sources.
    this.layers.forEach((config: LayerInputConfig) => {
      if (
        [
          'm-layers:line',
          'm-layers:road-section',
          'm-layers:cycle-path',
        ].includes(config.layer)
      ) {
        config.backgroundColor = !useDark ? '#fff' : '#000';
      }
      return config;
    });
    // Force Openlayers to call style functions and update styles.
    this.linesSource.changed();
    this.roadSectionSource.changed();
    this.poiLayerHelper.getSource().changed();
  }
  OSM(){    
    this.xYZDynamicSource.updateSource('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}');
    this.xYZDynamicSource.changed();
    this.tiles = JSON.parse(JSON.stringify(this.tiles)).map(
      (config: LayerInputConfig, index : number) => {
        config.visible = false;
        if(config.layer == 'core:dynamic') config.visible = true;
        return config;
      }
    );
  }

  updateRoadSection(): void {
    (this.disturbancesSource.getSource() as VectorSource).getFeatures().forEach(feature => {
      if(!feature.get('visibleVelo')) {
        feature.set('visibleMap', false);
      }
    });
    this.roadSectionSource.update();
  }

  zoomIn(): void {
    this.zoom += 1;
  }

  zoomOut(): void {
    this.zoom -= 1;
  }

  enableTracking(): void {
    this.geolocationSource.enable();
    this.tracking = true;
    // Instead of using the data binding to update the layer visibility you can use the map layer service.
    if (this.geolocationConfig && this.geolocationConfig._uid) {
      this.layersService.show(this.map, this.geolocationConfig._uid);
    }
  }

  disableTracking(): void {
    this.geolocationSource.disable();
    this.tracking = false;
    // Instead of using the data binding to update the layer visibility you can use the map layer service.
    if (this.geolocationConfig && this.geolocationConfig._uid) {
      this.layersService.hide(this.map, this.geolocationConfig._uid);
    }
  }

  updateCitizYea(): void {
    this.citizyeaSource.update().subscribe();
  }

  updateLetaxi(): any {
    this.letaxiSource.update().subscribe();
  }

  changeState(preConfig: string): void {    
    this.mapFiltersService
      .setState((this.mapFiltersService.data as any).preConfig[preConfig])
      .subscribe(() => {
        // Only for logs
        const poiSourceFeatures = [];
        (this.poiSource.getSource() as VectorSource)
          .getFeatures()
          .forEach((feature) => {           
            poiSourceFeatures.push({
              poiSourceFeature: feature.getProperties(),
            });
          });
          // this.linesSource.changed();
          // this.roadSectionSource.changed();
          // this.poiLayerHelper.getSource().changed();
      });
  }

  filtersChange(states: string[]): void {
    this.setState(states);
  }

  onMapClick(event: BaseEvent): void {
    if (this.selectedFeature) {
      this.selectedFeature.set('selected', false);
      this.selectedFeature = undefined;
    }
    if (event instanceof MapBrowserEvent) {
      event.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        if (!this.selectedFeature) {
          if (!feature.getProperties().visibleVoiture) {
            const feat: Feature = feature as Feature;
            feat.set('selected', true);

            // Keep taxi on map
            if (feat.getProperties().type === 'letaxi') {
              this.letaxiSource.keepTaxiOnMap(feat.getId() as string, true);
            }
            this.selectedFeature = feature;

            this.openDataDialog(feature);
          } else {
            const featureId = `disturbance_${feature.getProperties().code}`;
            const shapeFeatureId = `disturbance_${feature.getProperties().code}_shape`;
            (this.disturbancesSource.getSource() as VectorSource).getFeatures()
              .forEach(disturbance => {
                if ((disturbance.getId() as string) === shapeFeatureId) {
                  disturbance.set('visibleMap', true);
                  console.log({ disturbance, shapeFeatureId })
                }
              });
          }
        }
      });
    }
  }

  showLineDetails(): void {
    this.router.navigate(['/ligne-seule/' + this.lineID]).then();
  }

  async loadLine(idLigne: any): Promise<void> {
    const feature = this.linesService.find(idLigne);
    const lineState = `line:${feature.type}`;
    const clustersState = `poi:clusters`;
    const layerLine =
      this.mapFiltersService.getLayersFromState(lineState)[0] || undefined;
    const layerClusters =
      this.mapFiltersService.getLayersFromState(clustersState)[0] || undefined;
    if (layerLine) {
      layerLine.ids = [idLigne];
    }
    if (layerClusters) {
      layerClusters.ids = [...this.clusterIds];
    }
    this.mapFiltersService
      .setState([lineState, clustersState])
      .subscribe(() => {
        const lineFeature = (
          this.linesSource.getSource() as VectorSource
        ).getFeatureById(idLigne);
        if (lineFeature) {
          const extent = lineFeature.getGeometry().getExtent();
          if (extent) {
            this.fitView(extent);
          }
        }
      });
  }

  fitView(extent: Extent, offsetBottom = 0, duration = 1500): Observable<any> {
    const padding = 32;
    const fit$ = new Observable((obs) => {
      this.map.view.fit(extent, {
        padding: [padding + 16, padding, padding + offsetBottom, padding],
        duration,
        callback: (complete) => {
          obs.next(complete);
          obs.complete();
        },
      });
    }).pipe(shareReplay(1));
    publish()(fit$).connect();
    return fit$;
  }

  openDataDialog(feature: FeatureLike): void {
    this.dialog.open(DataDialComponent, {
      data: feature.getProperties(),
      panelClass: ['m-theme', 'full-screen-mobile'],
    });
  }

  public customAdresse(): void {
    if (this.formCustomAdresse.valid) {
      let coordinate: Coordinate = null;
      if (
        this.formCustomAdresse.value.lon === '' ||
        this.formCustomAdresse.value.lat === ''
      ) {
        coordinate = null;
      } else {
        coordinate = [
          this.formCustomAdresse.value.lon,
          this.formCustomAdresse.value.lat,
        ];
      }
      this.leTaxiService.customAdresse = coordinate;
      this.letaxiSource.update().subscribe();
    }
  }

  public resetAdresse(): void {
    this.leTaxiService.customAdresse = null;
  }

  private setState(states: string[], log = false): void {
    if (!log) {
      this.mapFiltersService.setState(states).subscribe();
    } else {
      this.mapFiltersService.setState(states).subscribe(() => {
        // Only for logs
        const poiSourceFeatures = [];
        (this.poiSource.getSource() as VectorSource)
          .getFeatures()
          .forEach((feature) => {
            console.log({ feature: feature.getProperties() });
          });
      });
    }
  }

  public disabledAllTaxi(): void {
    this.letaxiSource.leaveTaxi();
  }
}
