import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataTypesResolver, LinesResolver, MapFiltersResolver, ClustersResolver } from '@metromobilite/m-features/core';
import { HomeComponent } from './pages/home/home.component';
import { LigneSeuleComponent } from './pages/ligne-seule/ligne-seule.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {
      lines: LinesResolver,
    }
  },
  {
    path: 'ligne-seule/:id',
    component: LigneSeuleComponent,
    resolve: {
      lines: LinesResolver,
      clusters: ClustersResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
