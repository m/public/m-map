import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';

import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MFeaturesModule } from '@metromobilite/m-features';
import { CoreModule } from '@metromobilite/m-features/core';
import { RealtimeDataModule } from '@metromobilite/m-features/realtime-data';
import { MLayersModule } from '@metromobilite/m-layers';
import { MMapModule } from '@metromobilite/m-map';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataDialComponent } from './components/data-dial/data-dial.component';
import { HomeComponent } from './pages/home/home.component';
import { LigneSeuleComponent } from './pages/ligne-seule/ligne-seule.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LigneSeuleComponent,
    DataDialComponent,
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        HttpClientModule,
        AppRoutingModule,
        MMapModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        MatFormFieldModule,
        MatDialogModule,
        MFeaturesModule.forRoot({
            domain: 'https://data-pp.mobilites-m.fr'
            // domain: 'http://localhost:5400',
        }),
        CoreModule,
        MLayersModule,
        MatSlideToggleModule,
        MatListModule,
        MatTabsModule,
        MatCardModule,
        MatInputModule,
        MatCheckboxModule,
        MatExpansionModule,
        RealtimeDataModule,
        ReactiveFormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
