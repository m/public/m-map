import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-data-dial',
  templateUrl: './data-dial.component.html',
  styleUrls: ['./data-dial.component.scss'],
})
export class DataDialComponent {

  writtenData: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    // convertis la data en quelque chose de lisible facilement pour un utilisateur classique
    this.writtenData = '\n' + JSON.stringify(data, null, 4);
  }

}
