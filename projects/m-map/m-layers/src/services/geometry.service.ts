import { Inject, Injectable } from '@angular/core';
import { GeolocationSource, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
// Ol
import { Feature } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { Polyline } from 'ol/format';
import { LineString, MultiLineString, SimpleGeometry } from 'ol/geom';
import { transform } from 'ol/proj';
// Rxjs
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
    providedIn: 'root',
})
export class GeometryService {
    private polyline = new Polyline();

    constructor(
        @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
        @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string) { }

    getGeometry(shape: string | string[]): SimpleGeometry {
        if (Array.isArray(shape)) {
            return new MultiLineString(shape.map(s => {
                return this.getGeometry(s) as LineString;
            }));
        } else {
            return this.polyline.readGeometry(shape, {
                dataProjection: this.dataProjection,
                featureProjection: this.featureProjection
            }) as LineString;
        }
    }
}
