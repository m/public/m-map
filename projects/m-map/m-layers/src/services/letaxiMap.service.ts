import {Inject, Injectable} from '@angular/core';
import {GeolocationSource, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE} from '@metromobilite/m-map';
// Ol
import {Feature} from 'ol';
import {Coordinate} from 'ol/coordinate';
import {transform} from 'ol/proj';
// Rxjs
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class LetaxiMapService {
  // Get adresse data
  private customAdresse$: BehaviorSubject<Coordinate> = new BehaviorSubject<Coordinate>(null);
  // Custom adresse
  private customAdresseFeature: Feature = null;
  // Position of adresse in EPSG
  private position: Coordinate;

  constructor(private geolocationSource: GeolocationSource,
              @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
              @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string) {
  }

  public initData(): Observable<Coordinate> {
    this.position = null;
    this.customAdresseFeature = null;

    return combineLatest([
      this.geoLocationSource(),
      this.customAdresseObservable()
    ])
    .pipe(map(([geo, custom]) => {
      let result: number[] = geo;
      if (custom) {
        result = transform(custom, this.dataProjection, this.featureProjection);
      }
      return result;
    }));
  }

  // ===========================================================================
  // Geolocation Observable
  public geoLocationSource(): Observable<Coordinate> {
    return this.geolocationSource.position$.asObservable();
  }

  // ===========================================================================
  // Custom adresse Getter/Setter/Observable
  public get customAdresse(): Coordinate {
    return this.customAdresse$.getValue();
  }

  public set customAdresse(customAdresse: Coordinate) {
    this.customAdresse$.next(customAdresse);
  }

  public customAdresseObservable(): Observable<Coordinate> {
    return this.customAdresse$.asObservable();
  }

}
