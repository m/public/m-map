import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { LineStyleConfig, PlygonStyleConfig } from '../m-layers.model';
import { Fill } from 'ol/style';

@Injectable({ providedIn: 'root' })
export class MlayersPlygonStyle extends AbstractStyle {

  protected makeStyle(config: PlygonStyleConfig): Style | Style[] {
    const width = config.width || 3;
    const zOffset = config.zOffset || 0;
    const styles = [];

    styles.push(new Style({
      fill: new Fill({
        color: config.color,
      }),
      stroke: new Stroke({
        color: config.strokeColor || config.color,
        width,
        lineDash: config.lineDash || null
      }),
      zIndex: 1 + zOffset
    }));

    if (config.alternateColor && config.lineDash) {
      styles.push(new Style({
        stroke: new Stroke({
          color: config.alternateColor,
          width,
          lineDash: config.lineDash,
        }),
        zIndex: 2 + zOffset
      }));
    }

    return styles;
  }

}
