import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Circle from 'ol/style/Circle';
import { CircleStyleConfig } from '../m-layers.model';

@Injectable({ providedIn: 'root' })
export class MlayersCircleStyle extends AbstractStyle {

  protected makeStyle(config: CircleStyleConfig): Style | Style[] {
    return new Style({
      image: new Circle({
        radius: config.radius || 3,
        fill: new Fill({
          color: config.color
        }),
        stroke: new Stroke({
          color: config.strokeColor || '#000',
          width: config.strokeWidth || 1
        })
      }),
      zIndex: config.zIndex || 1,
    });
  }

}
