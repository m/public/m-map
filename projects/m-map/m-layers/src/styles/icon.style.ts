import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import { IconStyleConfig } from '../m-layers.model';

@Injectable({ providedIn: 'root' })
export class MlayersIconStyle extends AbstractStyle {

  protected makeStyle(config: IconStyleConfig): Style | Style[] {
    return new Style({
      image: new Icon({
        ...config.icon
      }),
      zIndex: config.zIndex
    });
  }

}
