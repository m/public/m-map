import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { LineStyleConfig } from '../m-layers.model';

@Injectable({ providedIn: 'root' })
export class MlayersLineStyle extends AbstractStyle {

  protected makeStyle(config: LineStyleConfig): Style | Style[] {
    const width = config.width || 3;
    const zOffset = config.zOffset || 0;
    const styles = [];
    if (!config.noBorder) {
      styles.push(new Style({
        stroke: new Stroke({
          color: config.backgroundColor || config.color,
          width: width + 2,
        }),
        zIndex: 1 + zOffset
      }));
    }
    styles.push(new Style({
      stroke: new Stroke({
        color: config.color,
        width,
        lineDash: config.lineDash || undefined,
      }),
      zIndex: 3 + zOffset
    }));

    if (config.alternateColor && config.lineDash && config.lineDashOffset) {
      styles.push(new Style({
        stroke: new Stroke({
          color: config.alternateColor,
          width,
          lineDash: config.lineDash,
          lineDashOffset: config.lineDashOffset,
        }),
        zIndex: 2 + zOffset
      }));
    }

    return styles;
  }

}
