import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Text from 'ol/style/Text';
import { TextStyleConfig } from '../m-layers.model';

@Injectable({ providedIn: 'root' })
export class MlayersTextStyle extends AbstractStyle {

  protected makeStyle(config: TextStyleConfig): Style | Style[] {
    return new Style({
      zIndex: config.zIndex || 0,
      text: new Text({
        font: config.font || '18px Roboto-Medium, Arial, sans-serif',
        text: config.text,
        textAlign: config.textAlign || 'start',
        textBaseline: config.textBaseline || 'middle',
        offsetX: config.offsetX || 0,
        offsetY: config.offsetY || 0,
        //scale:1,
        fill: new Fill({
          color: config.fillColor || 'rgba(0, 0, 0, 1)'
        }),
        stroke: new Stroke({
          color: config.strokeColor || '#ffffff',
          width: config.strokeWidth || 5
        })
      })
    });
  }

}
