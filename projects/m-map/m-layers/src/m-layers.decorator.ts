import { LayerHelperConfig, LAYER_HELPER_META } from './m-layers.model';

export function LayerHelper(config: LayerHelperConfig): (target: any) => void {
  return (target: any) => {
    Reflect.defineMetadata(LAYER_HELPER_META.CONFIG, config, target);
  };
}
