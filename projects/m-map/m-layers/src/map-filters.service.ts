import {Injectable} from '@angular/core';
import {ConfigService, FRONT_CONFIG, Interval, IntervalBuilderService, MapFilter, MapFiltersResponse} from '@metromobilite/m-features/core';
import {LayerInputConfig, MapComponent, MapLayersService, MapSourceInterface} from '@metromobilite/m-map';
import {Observable, of, Subject, zip} from 'rxjs';
import {catchError, map, publish, shareReplay, switchMap} from 'rxjs/operators';
import {MLayerHelpersService} from './layer-helpers.service';
import {LayerHelperConfig, StateChangeEvent, StateChangeEventType, StateConfig} from './m-layers.model';

@Injectable({ providedIn: 'root' })
export class MapFiltersService {

    data!: MapFiltersResponse;

    leafs: MapFilter[] = [];
    leafsMap: { [key: string]: MapFilter; } = {};
    layers: LayerInputConfig[] = [];
    initialized = false;
    currentStateConfig: StateConfig = {};
    private _currentState: string[] = [];


    public get currentState(): string[] {
        return Object.keys(this.currentStateConfig);
    }
    public set currentState(value: string[]) {
        this._currentState = value;
    }

    map!: MapComponent;
    updatePeriod = 60000;

    private interval?: Interval;
    private updates = new Subject<void>();

    constructor (private frontConfig: ConfigService,
        private layerHelpersService: MLayerHelpersService,
        private layersService: MapLayersService,
        private intervalBuilder: IntervalBuilderService) {
    }

    get updates$(): Observable<void> {
        return this.updates.asObservable();
    }

    /**
     * Init la map avec les layers
     */
    init(mapComponent: MapComponent, period = 60000): Observable<boolean> {
        this.map = mapComponent;
        this.updatePeriod = period;
        return this.frontConfig.getData(FRONT_CONFIG.MAP_FILTERS).pipe(
            map(response => {
                this.data = response;
                this.initialized = true;
                this.leafs = this.getLeaf();
                this.leafsMap = this.leafs.reduce((acc, leaf) => {
                    acc[leaf.id] = leaf;
                    return acc;
                }, {} as { [key: string]: MapFilter });
                this.layers = this.getLayers();
                return true;
            }),
            catchError(err => of(false)),
        );
    }

    /**
     * Return true if the state changes start updating data.
     */
    setState(stateIdentifiers: string[]): Observable<boolean> {
        let config: StateConfig = {};
        const states: string[] = [];

        stateIdentifiers.forEach(stateIdentifier => {
            const parts = stateIdentifier.split(':');
            let state = '';
            if (!stateIdentifier.includes('line')) {
                state = stateIdentifier;
            } else {
                state = parts.slice(0, 2).join(':');
            }

            states.push(state);

            if (!config[state]) config[state] = [];

            if (parts.length > 2 && state.includes('line')) {
                let lineID = parts.slice(2).join(':'); // Joining back the parts of lineID
                config[state].push(lineID);
            }
        });

        const { newStates, others } = states.reduce((acc, state) => {
            const tempStateConfig: StateConfig = { [state]: config[state] };
            if (this.currentStateConfig && Object.keys(this.currentStateConfig).includes(state)) {
                const leaf = this.leafsMap[state];
                if (leaf && leaf.update) {
                    acc.others = { ...acc.others, ...tempStateConfig };
                } else {
                    acc.newStates = { ...acc.newStates, ...tempStateConfig };
                }
            } else {
                acc.newStates = { ...acc.newStates, ...tempStateConfig };
            }
            return acc;
        }, { newStates: {}, others: {} } as { [key: string]: StateConfig });


        const otherFilters = Object.keys(others).map(o => this.leafsMap[o]);
        const oldStates = this.getOldStates(this.currentStateConfig, config);

        // Manage old states.
        const oldStateChangeEvents: StateChangeEvent[] = this.buildEvents(oldStates, 'remove', otherFilters);

        // Manage new state
        const addStateChangeEvents: StateChangeEvent[] = this.buildEvents(newStates, 'add', otherFilters);

        // Manage non changes that need update.
        const sourceNeedUpdates = Object.keys(others)
            .reduce((acc, e) => {
                const leaf = this.leafsMap[e];
                const layerName = this.layerHelpersService.configs[leaf.layer].layer;
                acc.push(this.layersService.layers[layerName].source);
                if (leaf.zone) {
                    const zoneLayerName = this.layerHelpersService.configs[leaf.zone].layer;
                    acc.push(this.layersService.layers[zoneLayerName].source);
                }
                return acc;
            }, [] as MapSourceInterface[])
            .filter(s => 'update' in s);

        const observable = this.triggerEvents(oldStateChangeEvents).pipe(
            switchMap(removedResponse => {
                if (this.interval) {
                    this.interval.destroy();
                    this.interval = undefined;
                }
                return this.triggerEvents(addStateChangeEvents).pipe(
                    map(addedResponse => {
                        this.currentStateConfig = config;
                        this.currentState = Object.keys(this.currentStateConfig);
                        const updates: Observable<boolean>[] = addedResponse.filter(response => typeof response !== 'boolean') as Observable<boolean>[];
                        if (sourceNeedUpdates.length > 0) {
                            for (const source of sourceNeedUpdates) {
                                updates.push((source as any).update());
                            }
                        }
                        if (updates.length > 0) {
                            this.interval = this.intervalBuilder.build(this.updatePeriod);
                            this.interval.on(zip(...updates)).subscribe(() => {
                                this.updates.next();
                            });
                        }
                        return updates.length > 0;
                    }),
                );
            }),
            shareReplay(1),
        );
        const published = publish<boolean>()(observable);
        // Trigger the events before subscription, (The shareReplay...)
        published.connect();
        return observable;
    }

    getLayersFromState(state: string): LayerInputConfig[] {
        return this.layers.filter(l => l.filterIds.includes(state));
    }

    private buildEvents(statesConfig: StateConfig, eventType: StateChangeEventType, others: MapFilter[] = []): StateChangeEvent[] {

        const states = Object.keys(statesConfig);
        return this.layers
            .filter(layerConfig => {
                return layerConfig.filterIds.some((id: string) => states.includes(id)) &&
                    this.layerHelpersService.reverse[layerConfig.layer];
            })
            .map(layerConfig => {
                const layers = [layerConfig];
                const data = states
                    .filter(s => layerConfig.filterIds.includes(s))
                    .map(s => {
                        let leaf = this.leafs.find(l => l.id === s) as MapFilter;
                        if (statesConfig[s] && statesConfig[s].length > 0) {
                            leaf = { ...leaf, lines: statesConfig[s] };
                        }
                        return leaf;
                    });
                for (const leaf of data) {
                    const possibleLayers = [this.layerHelpersService.configs[leaf.layer].layer];

                    if (leaf.zone && this.layerHelpersService.configs[leaf.zone]) {
                        possibleLayers.push(this.layerHelpersService.configs[leaf.zone].layer);
                    }
                    layers.push(...this.layers.filter(layer => possibleLayers.includes(layer.layer) && !layers.includes(layer)));
                }

                return {
                    event: eventType,
                    helperId: this.layerHelpersService.reverse[layerConfig.layer],
                    layers,
                    data,
                    map: this.map,
                    others,
                } as StateChangeEvent;
            })
            .reduce((acc, event) => {
                const foundEvent = acc.find((i: { helperId: string }) => i.helperId === event.helperId);
                if (!foundEvent) {
                    acc.push(event);
                } else {
                    foundEvent.data.push(...event.data);
                    foundEvent.layers.push(...event.layers);
                }
                return acc;
            }, [] as StateChangeEvent[]);
    }

    private triggerEvents(events: StateChangeEvent[]): Observable<(boolean | Observable<boolean>)[]> {
        const observables: Observable<boolean | Observable<boolean>>[] = [];
        for (const event of events) {
            const helper = this.layerHelpersService.helpers[event.helperId];
            if ('stateChange' in helper) {
                observables.push(helper.stateChange(event) as Observable<boolean | Observable<boolean>>);
            }
        }
        // If no stateChange return false (nothing to do);
        if (observables.length === 0) {
            observables.push(of(false));
            observables.push(of(false));
        }
        return zip(...observables);
    }

    private getLeaf(): MapFilter[] {
        if (!this.data) {
            return [];
        }
        const leafs: MapFilter[] = [];
        for (const rootFilter of this.data.filters) {
            this._getLeaf(leafs, rootFilter);
        }
        return leafs;
    }

    private _getLeaf(leafs: MapFilter[], filter: MapFilter): void {
        if (filter.layer !== undefined && filter.id !== undefined) {
            leafs.push(filter);
        } else {
            if (filter.children && filter.children.length > 0) {
                for (const child of filter.children) {
                    this._getLeaf(leafs, child);
                }
            }
        }
    }

    private getLayers(): LayerInputConfig[] {
        if (!this.leafs) {
            return [];
        }
        // extract layers from the map filter leafs.
        const layers: LayerInputConfig[] = [];
        for (const filter of this.leafs) {
            const configs = [];
            let config: LayerHelperConfig;
            if (filter.layer !== undefined) {
                config = this.layerHelpersService.configs[filter.layer];
                if (config && config.layer !== undefined) {
                    configs.push(config);
                }
            }
            if (filter.zone !== undefined) {
                config = this.layerHelpersService.configs[filter.zone];
                if (config && config.layer !== undefined) {
                    configs.push(config);
                }
            }
            for (const layerConfig of configs) {
                const shouldBeAdded = layerConfig.unique ? !layers.some(c => c.layer === layerConfig.layer) : true;
                if (shouldBeAdded && layerConfig.layer) {
                    const helper = this.layerHelpersService.helpers[layerConfig.id];
                    const lic = 'build' in helper ? helper.build(filter) : { layer: layerConfig.layer, visible: false };
                    lic.filterIds = [];
                    lic.filterIds.push(filter.id);
                    layers.push(lic);
                } else if (!shouldBeAdded && layerConfig.layer) {
                    const lic = layers.find(c => c.layer === layerConfig.layer);
                    if (lic) {
                        lic.filterIds.push(filter.id);
                    }
                }
            }
        }
        layers.sort((a, b) => {
            const aw = this.layerHelpersService.configs[this.layerHelpersService.reverse[a.layer]].weight;
            const bw = this.layerHelpersService.configs[this.layerHelpersService.reverse[b.layer]].weight;
            return aw > bw ? 1 : aw < bw ? -1 : 0;
        });
        const customSortHelper = Object.values(this.layerHelpersService.helpers).filter(helper => 'sortLayers' in helper);
        for (const helper of customSortHelper) {
            helper.sortLayers(layers, this.leafs);
        }
        return layers;
    }

    private getOldStates(currentStateConfig: StateConfig, newState: StateConfig): StateConfig {
        let oldStates: StateConfig = {};

        // Iterate over the keys of currentStateConfig
        for (let key in currentStateConfig) {
            // Check if the key is not present in newState
            if (!(key in newState)) {
                oldStates[key] = currentStateConfig[key];
            } else if ((key in newState)) {
              // Check if the key is present in newState but the values are different
                for (let line of currentStateConfig[key]) {
                    if (!newState[key].includes(line)) {
                        if (!oldStates[key]) oldStates[key] = [];
                        oldStates[key].push(line);
                    }
                }
            }
        }

        return oldStates;
    }

}
