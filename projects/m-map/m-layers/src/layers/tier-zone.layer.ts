import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { MLayersTierSource } from '../sources/tier.source';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:tier-zone')
export class MLayersTierZoneLayer extends MapLayerBase {

  constructor(public source: MLayersTierSource) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    const zoneStyle = new Style({
      stroke: new Stroke({
        color: [0, 0, 0, 1],
        width: 1,
      }),
      fill: new Fill({
        color: [188, 213, 47, 0.2]
      }),
    });
    return new VectorLayer({
      source: this.source.zoneSource,
      style: (feature, resolution) => {
        return zoneStyle;
      }
    });
  }

}
