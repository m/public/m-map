import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PoiStyleHelper } from '../poi-style.helper';
import { MLayersDisturbancesSource } from '../sources/disturbances.source';
import { MlayersLineStyle } from '../styles/line.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:disturbances')
export class MLayersDisturbancesLayer extends MapLayerBase {

  constructor (
    public source: MLayersDisturbancesSource,
    private lineStyle: MlayersLineStyle,
    private poiStyleHelper: PoiStyleHelper
  ) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const type = feature.get('type');
        const poi_zOffset = 6;
        if (type !== 'event_shape' && this.poiStyleHelper.isVisible(config, feature, resolution)) {
          return this.poiStyleHelper.getStyle(feature, resolution, component, poi_zOffset);
        } else if (type === 'event_shape' && feature.get('visibleMap')) {
          return this.lineStyle.build({ color: '#FFFF00', alternateColor: '#000000', lineDash: [10, 20], lineDashOffset: 15, width: 5 });
        }
        return [];
      }
    });
    return layer;
  }

}
