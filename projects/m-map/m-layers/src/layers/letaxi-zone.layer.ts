import {Injectable} from '@angular/core';
import {LayerInputConfig, MapComponent, MapLayer, MapLayerBase} from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import {Style} from 'ol/style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import {MLayersLetaxiSource} from '../sources/letaxi.source';

@Injectable({providedIn: 'root'})
@MapLayer('m-layers:letaxi')
export class MLayersLetaxiZoneLayer extends MapLayerBase {

  constructor(public source: MLayersLetaxiSource) {
    super();
  }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    // Create circle
    const newStyle = new Style({
      fill: new Fill({
        color: 'rgba(108,37,115,0.2)',
      }),
      stroke: new Stroke({
        color: 'rgba(58,19,61,0.6)',
        width: 2,
      }),
    });

    return new VectorLayer({
      source: this.source.zoneSource,
      style: (feature, resolution) => {
        return newStyle;
      },
    });
  }
}
