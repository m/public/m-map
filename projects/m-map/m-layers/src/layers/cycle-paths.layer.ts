import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { LineStyleConfig } from '../m-layers.model';
import { MLayersCyclePathsSource } from '../sources/cycle-paths.source';
import { MlayersLineStyle } from '../styles/line.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:cycle-path')
export class MLayersCyclePathLayer extends MapLayerBase {

  cyclePathStyles: { [key: string]: LineStyleConfig } = {
    chronovelo: { color: '#fdea00', width: 5, noBorder: false, backgroundColor: '#000000'},
    veloseparatif: { color: '#498d60', noBorder: true },
    veloconseille: { color: '#B5CC65', noBorder: true },
    velodifficile: { color: '#B5CC65', alternateColor: '#D12E26', lineDash: [10, 10], lineDashOffset: 10, noBorder: true },
  };

  constructor(
    public source: MLayersCyclePathsSource,
    private lineStyle: MlayersLineStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    if (typeof config === 'string' || !config.type) {
      throw new Error('Unknown type input configuration for the "m-layers:cycle-path" layer.');
    }
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const type = feature.get('type');
        if (type !== config.type) {
          return [];
        }
        return this.lineStyle.build(this.cyclePathStyles[type]);
      }
    });
    return layer;
  }

}
