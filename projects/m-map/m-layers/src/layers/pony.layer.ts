import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PoiStyleHelper } from '../poi-style.helper';
import { MLayersPonySource } from '../sources/pony.source';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:pony')
export class MLayersPonyLayer extends MapLayerBase {

  constructor(
    public source: MLayersPonySource,
    private poiStyleHelper: PoiStyleHelper
  ) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    return new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        if (this.poiStyleHelper.isVisible(config, feature, resolution)) {
          return this.poiStyleHelper.getStyle(feature, resolution, component);
        }
        return [];
      }
    });
  }

}
