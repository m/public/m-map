import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { MLayersRoadSectionSource } from '../sources/road-section.source';
import { MlayersLineStyle } from '../styles/line.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:road-section')
export class MLayersRoadSectionLayer extends MapLayerBase {

  nsvColorMap: { [key: number]: string } = {
    1: '#66CC33',
    2: '#FF6600',
    3: '#ff0000',
    4: '#000000',
  };

  constructor(
    public source: MLayersRoadSectionSource,
    private lineStyle: MlayersLineStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const lvl = feature.get('niveau');
        const nsv: number = feature.get('nsv') || 0;
        if (nsv === 0 || (lvl === 1 && resolution >= 10) || (lvl === 0 && resolution < 10)) {
          return [];
        }
        const color = this.nsvColorMap[nsv];
        return this.lineStyle.build({
          color,
          backgroundColor: typeof config === 'object' && config.backgroundColor || '#000',
          zOffset: nsv
        });
      }
    });
    return layer;
  }

}
