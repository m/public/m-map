import {Injectable} from '@angular/core';
import {LayerInputConfig, MapComponent, MapLayer, MapLayerBase} from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {PoiStyleHelper} from '../poi-style.helper';
import {MLayersPoiSource} from '../sources/poi.source';

@Injectable({providedIn: 'root'})
@MapLayer('m-layers:poi')
export class MLayersPoiLayer extends MapLayerBase {

  constructor(
    public source: MLayersPoiSource,
    private poiStyleHelper: PoiStyleHelper,
  ) {
    super();
  }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    if (!config.types) {
      throw new Error('Unknown types input configuration for the "m-layers:poi" layer.');
    }
    return new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        // Filter out any POI Features that's not included in the layer's "ids" property.
        if (config.ids && !config.ids.includes(feature.getId())) {
          return [];
        }
        if (this.poiStyleHelper.isVisible(config, feature, resolution)) {
          return this.poiStyleHelper.getStyle(feature, resolution, component);
        }
        return [];
      },
    });
  }

}
