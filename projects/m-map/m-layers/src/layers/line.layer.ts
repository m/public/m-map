import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { MLayersLinesSource } from '../sources/lines.source';
import { MlayersLineStyle } from '../styles/line.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-layers:line')
export class MLayersLineLayer extends MapLayerBase {

  constructor(
    public source: MLayersLinesSource,
    private lineStyle: MlayersLineStyle
  ) {
    super();
  }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    if (typeof config === 'string' || !config.type) {
      throw new Error('Unknown type input configuration for the "m-layers:line" layer.');
    }
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const type = feature.get('type');
        if (config.ids && !config.ids.includes(feature.getId())) {
          return [];
        }
        if (config.type === type) {
          return this.lineStyle.build({
            color: feature.get('color'),
            backgroundColor: config.backgroundColor || '#000'
          });
        }
        return [];
      }
    });
    layer.set('type', config.type);
    return layer;
  }

}
