import { Inject, Injectable } from '@angular/core';
import { LayerHelperConfig, LAYER_HELPER_META } from './m-layers.model';
import { M_LAYERS_LAYER_HELPER } from './m-layers.token';

@Injectable({ providedIn: 'root' })
export class MLayerHelpersService {

  helpers: { [key: string]: any };
  configs: { [key: string]: LayerHelperConfig } = {};
  reverse: { [key: string]: string } = {};

  constructor(
    @Inject(M_LAYERS_LAYER_HELPER) helpers: any[]
  ) {
    this.helpers = helpers.reduce((acc, helper) => {
      const config: LayerHelperConfig = Reflect.getMetadata(LAYER_HELPER_META.CONFIG, helper.constructor);
      acc[config.id] = helper;
      this.configs[config.id] = config;
      this.reverse[config.layer] = config.id;
      return acc;
    }, {});
  }

}
