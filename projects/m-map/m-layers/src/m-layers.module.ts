import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {CoreModule} from '@metromobilite/m-features/core';
import {DynModule} from '@metromobilite/m-features/dyn';
import {MAP_LAYER} from '@metromobilite/m-map';

import {CitizYeaLayerHelper} from './layer-helpers/citizyea.layer-helper';
import {CyclePathLayerHelper} from './layer-helpers/cycle-path.layer-helper';
import {DisturbancesLayerHelper} from './layer-helpers/disturbances.layer-helper';
import {DottLayerHelper} from './layer-helpers/dott.layer-helper';
import {LetaxiLayerHelper} from './layer-helpers/letaxi.layer-helper';
import {LineLayerHelper} from './layer-helpers/line.layer-helper';
import {PoiLayerHelper} from './layer-helpers/poi.layer-helper';
import {PonyLayerHelper} from './layer-helpers/pony.layer-helper';
import {RoadSectionLayerHelper} from './layer-helpers/road-section.layer-helper';
import {TierLayerHelper} from './layer-helpers/tier.layer-helper';
import {MLayersCitizyeaZoneLayer} from './layers/citizyea-zone.layer';
import {MLayersCyclePathLayer} from './layers/cycle-paths.layer';
import {MLayersDisturbancesLayer} from './layers/disturbances.layer';
import {MLayersDottZoneLayer} from './layers/dott-zone.layer';
import {MLayersLetaxiZoneLayer} from './layers/letaxi-zone.layer';
import {MLayersLineLayer} from './layers/line.layer';
import {MLayersPoiLayer} from './layers/poi.layer';
import {MLayersPonyLayer} from './layers/pony.layer';
import {MLayersRoadSectionLayer} from './layers/road-section.layer';
import {MLayersTierZoneLayer} from './layers/tier-zone.layer';
import {availableCyclePaths, availableLineTypes} from './m-layers.data';
import {M_LAYERS_AVAILABLE_CYCLE_PATHS, M_LAYERS_AVAILABLE_LINE_TYPES, M_LAYERS_LAYER_HELPER} from './m-layers.token';

@NgModule({
  imports: [HttpClientModule, CoreModule, DynModule], providers: [
    {provide: M_LAYERS_AVAILABLE_LINE_TYPES, useValue: availableLineTypes},
    {provide: M_LAYERS_AVAILABLE_CYCLE_PATHS, useValue: availableCyclePaths},
    {provide: MAP_LAYER, useClass: MLayersLineLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersRoadSectionLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersCyclePathLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersLetaxiZoneLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersPoiLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersCitizyeaZoneLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersDisturbancesLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersTierZoneLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersDottZoneLayer, multi: true},
    {provide: MAP_LAYER, useClass: MLayersPonyLayer, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: LineLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: LetaxiLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: PoiLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: CyclePathLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: RoadSectionLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: CitizYeaLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: DisturbancesLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: TierLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: DottLayerHelper, multi: true},
    {provide: M_LAYERS_LAYER_HELPER, useClass: PonyLayerHelper, multi: true}
  ],
})

export class MLayersModule {
}
