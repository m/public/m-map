import { Injectable } from '@angular/core';
import {
  ApiService,
  ConfigService,
  DataType,
  DomainService,
  FRONT_CONFIG,
  SubtypesHelper,
} from '@metromobilite/m-features/core';
import { LayerInputConfig, MapComponent } from '@metromobilite/m-map';
import { FeatureLike } from 'ol/Feature';
import { MlayersCircleStyle } from './styles/circle.style';
import { MlayersIconStyle } from './styles/icon.style';

@Injectable({ providedIn: 'root' })
export class PoiStyleHelper {
  public domain: string;
  public api: string;

  constructor(
    private iconStyle: MlayersIconStyle,
    private circleStyle: MlayersCircleStyle,
    private frontConfig: ConfigService,
    private subtypesHelper: SubtypesHelper,
    private domainService: DomainService,
    private apiService: ApiService
  ) {
    this.domain = this.domainService.getDomain();
    this.api = this.apiService.getApi();
  }

  isVisible(
    config: LayerInputConfig,
    feature: FeatureLike,
    resolution: number
  ): boolean {
    if (feature.get('visibleMap') === false) {
      return false;
    }
    const type = feature.get('type');
    const subtype = this.subtypesHelper.get(feature);
    if (
      config.types && 
      (!config.types.includes(type) || (subtype !== '' && !config.subtypes.includes(subtype)))
    ) {
      return false;
    }
    const data =
      this.frontConfig.config[FRONT_CONFIG.DATA_TYPES] &&
      (this.frontConfig.config[FRONT_CONFIG.DATA_TYPES][type] as DataType);
    if (!data) {
      return false;
    }
    let visible = data.iconMaxRes ? resolution < data.iconMaxRes : true;
    if (visible && data.iconMinRes) {
      visible = resolution > data.iconMinRes;
    }
    return typeof feature.get('visible') !== 'undefined'
      ? feature.get('visible')
      : !visible && type === 'clusters'
      ? true
      : visible;
  }

  getStyle(feature: FeatureLike, resolution: number, map: MapComponent, zOffset = 1): any {
    let type = feature.get('type');
    // check in which mode we are so we can change style accordingly
    const useDark = map.tiles
      .filter((config) => config.visible)
      .reduce((_, config) => config.layer.includes('dark'), true);
    const subtype = this.subtypesHelper.get(feature);
    if (subtype !== '') {
      type = subtype;
    }
    const data = this.frontConfig.config[FRONT_CONFIG.DATA_TYPES][
      type
    ] as DataType;

    //  the zoom is too far, we use dots
    if (type === 'clusters' && resolution > 5) {
      const vals = {
        radius: 3,
        color: [250, 250, 250, 0.5],
        strokeColor: '#000',
        strokeWidth: 1,
      };
      const isSelected = feature.get('selected') || false;
      // change color of the dot if it's selected, and depending on the mode
      if (isSelected) {
        vals.radius = 6;
        vals.strokeColor = useDark ? '#b49bda' : '#8154c0';
        vals.strokeWidth = 2;
      }
      return this.circleStyle.build(vals);
    }

    // if point is selected change their size
    const isSelected = feature.get('selected') || false;

    // if their is icons, we use icons instead
    if (data.icon) {
      let icone = data.icon;

      if (isSelected && data.selected) icone = data.selected;

      icone.anchor = !icone.anchor ? data.icon.anchor : icone.anchor;
      const icon = JSON.parse(JSON.stringify(icone));
      const nsv = feature.get('nsv') || 0;
      const orientation = feature.get('CAP') || 0;
      (icon as any).rotation = (Math.PI / 180) * orientation;
      const periode = feature.get('periode') || undefined;
      icon.src = icon.src.replace(/\_[0-4].png$/g, '_' + nsv + '.png');

      // change if we use dark mode or light one
      icon.src = icon.src.replace(
        useDark ? '_light' : '_dark',
        useDark ? '_dark' : '_light'
      );
      if (!icon.src.includes(this.domain)) {
        icon.src = `${this.domain}/${this.api}${icon.src}`;
      }

      let opacity = periode && periode.bientot ? 0.6 : 1;
      if (nsv === 0 && orientation) {
        opacity = 0.25;
      }
      (icon as any).opacity = opacity;
      return this.iconStyle.build({
        icon,
        zIndex: isSelected ? 1000 : zOffset,
        selected: isSelected,
      });
    }
    return [];
  }
}
