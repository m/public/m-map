export const availableLineTypes = [
  'TRAM',
  'CHRONO',
  'PROXIMO',
  'FLEXO',
  'C38',
  'Urbaines',
  'Interurbaines',
  'Structurantes',
  'Secondaires',
];

export const availableCyclePaths = [
  'chronovelo',
  'veloseparatif',
  'veloconseille',
  'velodifficile',
];
