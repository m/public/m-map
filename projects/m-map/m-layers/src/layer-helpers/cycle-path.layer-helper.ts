import { Injectable } from '@angular/core';
import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig } from '@metromobilite/m-map';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MLayersCyclePathLayer } from '../layers/cycle-paths.layer';
import { LayerHelper } from '../m-layers.decorator';
import { LayerHelperBuild, LayerHelperSortLayers, LayerHelperStateChange, StateChangeEvent } from '../m-layers.model';
import { MLayersCyclePathsSource } from '../sources/cycle-paths.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 1,
  unique: false,
  layer: 'm-layers:cycle-path',
  id: 'cycle-path'
})
export class CyclePathLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperSortLayers {

  constructor(
    private layerService: MLayersCyclePathLayer,
    private source: MLayersCyclePathsSource
  ) { }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const types = event.data.map(d => d.type);
    const layers = event.layers.filter(layer => types.includes(layer.type));
    if (event.event === 'add') {
      return this.source.getData().pipe(
        map(() => {
          for (const layerInputConfig of layers) {
            this.layerService.show(event.map, layerInputConfig._uid as string);
          }
          return false;
        })
      );
    } else {
      for (const layerInputConfig of layers) {
        this.layerService.hide(event.map, layerInputConfig._uid as string);
      }
    }
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:cycle-path', visible: false, type: config.type };
  }

  sortLayers(layers: LayerInputConfig[], leafs: MapFilter[]): void {
    const configs = leafs.filter(l => l.layer === 'cycle-path');

    // Specific sort apply to cycle-path layers.
    layers.sort((a, b) => {
      if (a.layer === 'm-layers:cycle-path' && b.layer === 'm-layers:cycle-path') {
        const aw = configs.find(c => c.type === a.type && c.bikeLayerWeight !== undefined) || { bikeLayerWeight: 1 };
        const bw = configs.find(c => c.type === b.type && c.bikeLayerWeight !== undefined) || { bikeLayerWeight: 1 };
        return aw.bikeLayerWeight > bw.bikeLayerWeight ? 1 : aw.bikeLayerWeight < bw.bikeLayerWeight ? -1 : 0;
      }
      return 0;
    });
  }

}
