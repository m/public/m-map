import { Injectable } from '@angular/core';
import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig, MapSourceInterface } from '@metromobilite/m-map';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MLayersCitizyeaZoneLayer } from '../layers/citizyea-zone.layer';
import { LayerHelper } from '../m-layers.decorator';
import { LayerHelperBuild, LayerHelperSource, LayerHelperStateChange, StateChangeEvent } from '../m-layers.model';
import { MLayersCitizyeaSource } from '../sources/citizyea.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 3,
  unique: true,
  layer: 'm-layers:citizyea-zone',
  id: 'citizyea'
})
export class CitizYeaLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperSource {

  constructor(
    private layerService: MLayersCitizyeaZoneLayer,
    private yeaSource: MLayersCitizyeaSource
  ) { }

  getSource(): MapSourceInterface {
    return this.yeaSource;
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    if (event.event === 'add') {
      return this.yeaSource.getData().pipe(
        map(() => {
          this.layerService.show(event.map, layer._uid as string);
          this.yeaSource.changed();
          return this.yeaSource.update();
        })
      );
    }
    this.yeaSource.changed();
    this.layerService.hide(event.map, layer._uid as string);
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:citizyea-zone', visible: false };
  }

}
