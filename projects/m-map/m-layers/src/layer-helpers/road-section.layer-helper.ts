import { Injectable } from '@angular/core';
import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig } from '@metromobilite/m-map';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MLayersRoadSectionLayer } from '../layers/road-section.layer';
import { LayerHelper } from '../m-layers.decorator';
import { LayerHelperBuild, LayerHelperStateChange, StateChangeEvent } from '../m-layers.model';
import { MLayersRoadSectionSource } from '../sources/road-section.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 2,
  unique: true,
  layer: 'm-layers:road-section',
  id: 'road-section'
})
export class RoadSectionLayerHelper implements LayerHelperBuild, LayerHelperStateChange {

  constructor(
    private source: MLayersRoadSectionSource,
    private layerService: MLayersRoadSectionLayer
  ) { }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    if (event.event === 'add') {
      return this.source.getData().pipe(
        map(() => {
          for (const layerInputConfig of event.layers) {
            this.layerService.show(event.map, layerInputConfig._uid as string);
          }
          return this.source.update();
        })
      );
    } else {
      for (const layerInputConfig of event.layers) {
        this.layerService.hide(event.map, layerInputConfig._uid as string);
      }
    }
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:road-section', visible: false };
  }

}
