import { Injectable } from '@angular/core';
import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig, MapSourceInterface } from '@metromobilite/m-map';
import { Feature } from 'ol';
import { Extent } from 'ol/extent';
import Geometry from 'ol/geom/Geometry';
import VectorSource from 'ol/source/Vector';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MLayersDisturbancesLayer } from '../layers/disturbances.layer';
import { LayerHelper } from '../m-layers.decorator';
import { LayerHelperBuild, LayerHelperSource, LayerHelperStateChange, LayerHelperVisibleFeatures, StateChangeEvent } from '../m-layers.model';
import { PoiStyleHelper } from '../poi-style.helper';
import { MLayersDisturbancesSource } from '../sources/disturbances.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 200,
  unique: true,
  layer: 'm-layers:disturbances',
  id: 'disturbances'
})
export class DisturbancesLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperVisibleFeatures, LayerHelperSource {

  constructor(
    private layerService: MLayersDisturbancesLayer,
    private disturbancesSource: MLayersDisturbancesSource,
    private poiStyleHelper: PoiStyleHelper
  ) { }

  getSource(): MapSourceInterface {
    return this.disturbancesSource;
  }

  getVisibleFeatures(layer: LayerInputConfig, extent: Extent, resolution: number): Feature<Geometry>[] {
    const features = (this.disturbancesSource.getSource() as VectorSource).getFeaturesInExtent(extent);
    return features.filter(f => this.poiStyleHelper.isVisible(layer, f, resolution));
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    if (event.event === 'add') {
      return this.disturbancesSource.getData().pipe(
        map(response => {
          this.layerService.show(event.map, layer._uid as string);
          return this.disturbancesSource.update();
        })
      );
    }
    this.layerService.hide(event.map, layer._uid as string);
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:disturbances', visible: false };
  }

}
