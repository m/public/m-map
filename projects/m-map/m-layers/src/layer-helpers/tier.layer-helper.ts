import { Injectable } from '@angular/core';
import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig, MapSourceInterface } from '@metromobilite/m-map';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MLayersTierZoneLayer } from '../layers/tier-zone.layer';
import { LayerHelper } from '../m-layers.decorator';
import { LayerHelperBuild, LayerHelperSource, LayerHelperStateChange, StateChangeEvent } from '../m-layers.model';
import { MLayersTierSource } from '../sources/tier.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 4,
  unique: true,
  layer: 'm-layers:tier-zone',
  id: 'tier'
})
export class TierLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperSource {

  constructor(
    private layerService: MLayersTierZoneLayer,
    private source: MLayersTierSource
  ) { }

  getSource(): MapSourceInterface {
    return this.source;
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    if (event.event === 'add') {
      return this.source.getData().pipe(
        map(() => {
          this.layerService.show(event.map, layer._uid as string);
          this.source.changed();
          return this.source.update();
        })
      );
    }
    this.source.changed();
    this.layerService.hide(event.map, layer._uid as string);
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:tier-zone', visible: false };
  }

}
