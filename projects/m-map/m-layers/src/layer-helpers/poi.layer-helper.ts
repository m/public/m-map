import {Injectable} from '@angular/core';
import {MapFilter} from '@metromobilite/m-features/core';
import {LayerInputConfig, MapSourceInterface} from '@metromobilite/m-map';
import {Feature} from 'ol';
import {Extent} from 'ol/extent';
import Geometry from 'ol/geom/Geometry';
import VectorSource from 'ol/source/Vector';
import {Observable, of} from 'rxjs';
import {LayerHelper} from '../m-layers.decorator';
import {
  LayerHelperBuild,
  LayerHelperSource,
  LayerHelperStateChange,
  LayerHelperVisibleFeatures,
  StateChangeEvent,
} from '../m-layers.model';
import {PoiStyleHelper} from '../poi-style.helper';
import {MLayersPoiSource} from '../sources/poi.source';

@Injectable({providedIn: 'root'})
@LayerHelper({
  weight: 100,
  unique: true,
  layer: 'm-layers:poi',
  id: 'poi',
})
export class PoiLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperVisibleFeatures, LayerHelperSource {

  constructor(
    private source: MLayersPoiSource,
    private poiStyleHelper: PoiStyleHelper,
  ) {
  }

  getSource(): MapSourceInterface {
    return this.source;
  }

  getVisibleFeatures(layer: LayerInputConfig, extent: Extent, resolution: number): Feature<Geometry>[] {
    const features = (this.source.getSource() as VectorSource).getFeaturesInExtent(extent);
    return features.filter(f => this.poiStyleHelper.isVisible(layer, f, resolution));
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    const types: string[] = event.data.map(elt => elt.types).reduce((acc, typesToFlatten: string[]) => {
      acc.push(...typesToFlatten.filter(t => !acc.includes(t)));
      return acc;
    }, []);
    // Set subtypes (parking case...).
    layer.subtypes = event.data.filter(d => d.subtype !== undefined).map(d => d.subtype);
    if (event.event === 'add') {
      // Sometimes (eg. citizyea or other special case), we need to take into account others property.
      // The others property contains some leaf items from the filter tree that exist before this add event and are not removed.
      for (const other of event.others) {
        if (other.layer === 'poi') {
          for (const type of other.types) {
            if (!types.includes(type)) {
              types.push(type);
            }
          }
        }
      }
      layer.types = types;
    } else {
      layer.types = layer.types.filter((type: string) => !types.includes(type));
    }
    this.source.changed();
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return {layer: 'm-layers:poi', visible: true, types: [], subtypes: []};
  }

}
