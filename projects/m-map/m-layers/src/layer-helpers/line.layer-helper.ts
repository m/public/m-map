import {Injectable} from '@angular/core';
import {MapFilter} from '@metromobilite/m-features/core';
import {LayerInputConfig} from '@metromobilite/m-map';
import {Feature} from 'ol';
import {Geometry} from 'ol/geom';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {MLayersLineLayer} from '../layers/line.layer';
import {LayerHelper} from '../m-layers.decorator';
import {LayerHelperBuild, LayerHelperStateChange, StateChangeEvent} from '../m-layers.model';
import {MLayersLinesSource} from '../sources/lines.source';

@Injectable({ providedIn: 'root' })
@LayerHelper({
  weight: 0,
  unique: false,
  layer: 'm-layers:line',
  id: 'line'
})
export class LineLayerHelper implements LayerHelperBuild, LayerHelperStateChange {

  constructor(
    private linesSource: MLayersLinesSource,
    private layerService: MLayersLineLayer
  ) { }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const types = event.data.map(d => d.type);
    const lines = event.data.reduce((accumulator, currentValue) => {
      // Check if the 'lines' property exists and is not undefined
      if (currentValue.lines && Array.isArray(currentValue.lines)) {
        return accumulator.concat(currentValue.lines);
      }
      // If 'lines' doesn't exist or isn't an array, just return the accumulator as is
      return accumulator;
    }, [] as string[]);

    const mode = (lines && lines.length > 0) ? 'line' : 'type';
    const dataToLoad = (lines && lines.length > 0) ? lines : types;
    const layers = event.layers.filter(layer => types.includes(layer.type));

    if (event.event === 'add') {
      return this.linesSource.setLineTypes(dataToLoad, mode).pipe(
        map(response => {
          if (response) {
            for (const layerInputConfig of layers) {
              this.layerService.show(event.map, layerInputConfig._uid as string);
            }
          }
          return false;
        })
      );
    } else {
      for (const layerInputConfig of layers) {
        if (lines && lines.length > 0) {
          lines.forEach((line: string) => {
            const feature: Feature<Geometry> = this.linesSource.find(line);
            if (feature) this.linesSource.remove(feature);
          });
        }

        this.layerService.hide(event.map, layerInputConfig._uid as string);
      }
      return of(false);
    }
  }

  build(config: MapFilter): LayerInputConfig {
    return { layer: 'm-layers:line', visible: false, type: config.type };
  }

}
