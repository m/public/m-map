import {Injectable} from '@angular/core';
import {MapFilter} from '@metromobilite/m-features/core';
import {LayerInputConfig, MapSourceInterface} from '@metromobilite/m-map';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {MLayersDottZoneLayer} from '../layers/dott-zone.layer';
import {LayerHelper} from '../m-layers.decorator';
import {LayerHelperBuild, LayerHelperSource, LayerHelperStateChange, StateChangeEvent} from '../m-layers.model';
import {MLayersDottSource} from '../sources/dott.source';

@Injectable({providedIn: 'root'})
@LayerHelper({
  weight: 4,
  unique: false,
  layer: 'm-layers:dott-zone',
  id: 'dott',
})
export class DottLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperSource {

  constructor(
    private layerService: MLayersDottZoneLayer,
    private source: MLayersDottSource,
  ) {
  }

  getSource(): MapSourceInterface {
    return this.source;
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    if (event.event === 'add') {
      return this.source.getData().pipe(
        map(() => {
          this.layerService.show(event.map, layer._uid as string);
          this.source.changed();
          return this.source.update();
        }),
      );
    }
    this.source.changed();
    this.layerService.hide(event.map, layer._uid as string);
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return {layer: 'm-layers:dott-zone', visible: false};
  }

}
