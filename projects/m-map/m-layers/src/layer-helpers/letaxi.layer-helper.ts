import {Injectable} from '@angular/core';
import {MapFilter} from '@metromobilite/m-features/core';
import {LayerInputConfig, MapSourceInterface} from '@metromobilite/m-map';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {MLayersLetaxiZoneLayer} from '../layers/letaxi-zone.layer';
import {LayerHelper} from '../m-layers.decorator';
import {LayerHelperBuild, LayerHelperSource, LayerHelperStateChange, StateChangeEvent} from '../m-layers.model';
import {MLayersLetaxiSource} from '../sources/letaxi.source';

@Injectable({ providedIn: 'root' }) @LayerHelper({
  weight: 3, unique: true, layer: 'm-layers:letaxi', id: 'letaxi',
})
export class LetaxiLayerHelper implements LayerHelperBuild, LayerHelperStateChange, LayerHelperSource {

  constructor (private letaxiSource: MLayersLetaxiSource, private layerService: MLayersLetaxiZoneLayer) {
  }

  getSource(): MapSourceInterface {
    return this.letaxiSource;
  }

  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>> {
    const layer = event.layers[0];
    if (event.event === 'add') {
      return this.letaxiSource.getData().pipe(map(() => {
        this.layerService.show(event.map, layer._uid as string);
        this.letaxiSource.changed();
        return this.letaxiSource.update();
      }));
    }
    this.letaxiSource.changed();
    this.layerService.hide(event.map, layer._uid as string);
    return of(false);
  }

  build(config: MapFilter): LayerInputConfig {
    return {layer: 'm-layers:letaxi', visible: false};
  }

}
