import { Inject, Injectable } from '@angular/core';
import { TierCollection, TierService } from '@metromobilite/m-features/dyn';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import { transform } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import { Observable, of, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MLayersPoiSource } from './poi.source';

@Injectable({ providedIn: 'root' })
export class MLayersTierSource extends MapSourceBase {

  source: VectorSource;
  zoneSource = new VectorSource();
  private geoJSON = new GeoJSON();

  constructor(
    private tierService: TierService,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    source: MLayersPoiSource
  ) {
    super();
    this.source = source.getSource() as VectorSource;
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    const requests: Array<Observable<boolean>> = [];
    if (this.zoneSource.getFeatures().length === 0) {
      requests.push(
        this.tierService.getZone().pipe(
          map(response => {
            const features = response.features.map(f => {
              const geometry = this.geoJSON.readGeometry(f.geometry, {
                dataProjection: this.dataProjection,
                featureProjection: this.featureProjection
              });
              const feature = new Feature({
                geometry,
              });
              feature.setId(`vitesselimitee_${f.properties.id}`);
              return feature;
            }).filter((f, index) => index < 200);
            this.zoneSource.addFeatures(features);
            return true;
          }),
          catchError(err => of(false))
        )
      );
    }
    requests.push(this.tierService.getData().pipe(
      map(response => {
        this.handleResponse(response);
        return true;
      }),
      catchError(err => of(false))
    ));
    return zip(...requests).pipe(
      map(([zone, feature]) => {
        return zone && feature;
      })
    );
  }

  update(): Observable<boolean> {
    return this.tierService.getData().pipe(
      map(response => {
        this.handleResponse(response);
        return true;
      }),
      catchError(err => of(false))
    );
  }

  private handleResponse(response: TierCollection): void {
    for (const responseFeature of response.features) {
      const type = responseFeature.properties.type;
      const featureId = `${type}_${responseFeature.properties.code}`;
      const geometry = new Point(transform(responseFeature.geometry.coordinates, this.dataProjection, this.featureProjection));
      const f = this.source.getFeatureById(featureId);
      if (!f) {
        const feature = new Feature({
          geometry,
          ...responseFeature.properties
        });
        feature.setId(featureId);
        this.push(feature);
      } else {
        f.setGeometry(geometry);
        f.setProperties({ ...f.getProperties(), ...responseFeature.properties });
      }
    }
    // Remove old features
    const currentIds = response.features.map(f => `${f.properties.type}_${f.properties.code}`);
    const toRemove = this.source.getFeatures()
      .filter(f => f.get('type') === 'tierVehicle')
      .map(f => f.getId())
      .filter(i => !currentIds.includes(i as string));
    for (const id of toRemove) {
      this.source.removeFeature(this.source.getFeatureById(id as string));
    }
  }

}
