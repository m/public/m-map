import {Inject, Injectable} from '@angular/core';
import {DottService, GbfsCollection} from '@metromobilite/m-features/dyn';
import {MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase} from '@metromobilite/m-map';
import {Feature} from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import {fromLonLat} from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import {Observable, of, zip} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {MLayersPoiSource} from './poi.source';

@Injectable({providedIn: 'root'})
export class MLayersDottSource extends MapSourceBase {

  source: VectorSource;
  zoneSource = new VectorSource();
  private geoJSON = new GeoJSON();

  constructor(
    private dottService: DottService,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    source: MLayersPoiSource,
  ) {
    super();
    this.source = source.getSource() as VectorSource;
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    const requests: Array<Observable<boolean>> = [];
    if (this.zoneSource.getFeatures().length === 0) {
      requests.push(
        this.dottService.getZone().pipe(
          map(response => {
            const features = response.features.map(f => {
              const geometry = this.geoJSON.readGeometry(f.geometry, {
                dataProjection: this.dataProjection,
                featureProjection: this.featureProjection,
              });
              const feature = new Feature({
                geometry,
              });
              feature.setId(`vitesselimitee_${f.properties.id}`);
              return feature;
            }).filter((f, index) => index < 200);
            this.zoneSource.addFeatures(features);
            return true;
          }),
          catchError(err => of(false)),
        ),
      );
    }
    requests.push(this.dottService.getData().pipe(
      map(response => {
        this.handleResponse(response);
        return true;
      }),
      catchError(err => of(false)),
    ));
    return zip(...requests).pipe(
      map(([zone, feature]) => {
        return zone && feature;
      }),
    );
  }

  update(): Observable<boolean> {
    return this.dottService.getData().pipe(
      map(response => {
        this.handleResponse(response);
        return true;
      }),
      catchError(err => of(false)),
    );
  }

  private handleResponse(response: GbfsCollection): void {
    for (const bike of response.data.bikes) {
      const featureId = `dott_${bike.bike_id}`;
      let feature = this.source.getFeatureById(featureId);
      const geometry = new Point(fromLonLat([bike.lon, bike.lat]));

      if (bike.is_disabled || bike.is_reserved) continue;

      if (!feature) {
        feature = new Feature({
          ...bike,
          visibleMap: true,
          type: 'dott',
        });
        feature.setGeometry(geometry);
        feature.setId(featureId);
        this.push(feature);
      } else {
        feature.setGeometry(geometry);
        feature.setProperties(bike);
      }
    }
    // Remove old features
    const currentIds = response.data.bikes.map(f => `dott_${f.bike_id}`);
    const toRemove = this.source.getFeatures()
    .filter(f => f.get('type') === 'dott')
    .filter(i => !currentIds.includes(i.getId() as string));
    for (const feature of toRemove) {
      this.remove(feature);
    }
  }

}
