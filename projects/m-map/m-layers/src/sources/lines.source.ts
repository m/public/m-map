import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { LinesService } from '@metromobilite/m-features/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import Polyline from 'ol/format/Polyline';
import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import VectorSource from 'ol/source/Vector';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { M_LAYERS_AVAILABLE_LINE_TYPES } from '../m-layers.token';
import { LineToAddMode } from '../m-layers.model';
import { GeometryService } from '../public_api';

@Injectable({ providedIn: 'root' })
export class MLayersLinesSource extends MapSourceBase {

  source: VectorSource;

  private lineTypes: string[] = [];
  private polyline = new Polyline();

  constructor (
    private http: HttpClient,
    private linesService: LinesService,
    @Inject(M_LAYERS_AVAILABLE_LINE_TYPES) public availableLineTypes: string[],
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private geometryService: GeometryService
  ) {
    super();
    this.source = new VectorSource({});
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  setLineTypes(data: string[], mode: LineToAddMode = 'type'): Observable<boolean> {
    let linesToAdd: string[] = [];
    if (mode === 'line') {
      linesToAdd = data;
    } else {
      ({data})
      this.lineTypes = data;
      linesToAdd = this.linesService.lines
        .filter(line => this.lineTypes.includes(line.type) && !this.source.getFeatureById(line.id))
        .map(line => line.id);
    }
    ({ linesToAdd });

    if (linesToAdd.length === 0) {
      return of(true);
    }

    return this.addLinesToSource(linesToAdd);
  }

  private addLinesToSource(linesToAdd: string[]): Observable<boolean> {
    return this.http.get<{ features: { properties: { id: string; shape: string[] | string; }; }[]; }>(`@domain/@api/lines/poly`, {
      params: new HttpParams({
        fromObject: {
          types: 'ligne',
          codes: linesToAdd.join(',')
        }
      })
    }).pipe(
      map(response => {
        for (const feature of response.features) {
          const geometry = this.geometryService.getGeometry(feature.properties.shape);
          const line = this.linesService.linesAsMap[feature.properties.id.replace('_', ':')];
          const f = new Feature({
            geometry,
            type: line.type,
            color: `#${line.color}`,
            textColor: `#${line.textColor}`,
          });
          f.setId(line.id);
          this.push(f);
        }
        return true;
      }),
      catchError(err => of(false))
    );
  }
}
