import { Inject, Injectable } from '@angular/core';
import { DateHelper } from '@metromobilite/m-features/core';
import { DisturbancesService } from '@metromobilite/m-features/dyn';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Polyline from 'ol/format/Polyline';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector';
import { transform } from 'ol/proj';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MLayersDisturbancesSource extends MapSourceBase {

  source = new VectorSource();

  private polyline = new Polyline();

  constructor(
    private disturbancesService: DisturbancesService,
    private dateHelper: DateHelper,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string
  ) {
    super();
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    return this.disturbancesService.getData().pipe(
      map(response => {
        const currentFeatures = this.source.getFeatures();
        currentFeatures.forEach((feature) => {
          feature.set('keep', false);
        });
        const disturbances = Object.entries(response).filter(([key, d]) => d.latitude !== -1 && d.longitude !== -1);
        for (const [code, disturbance] of disturbances) {
          const featureId = `disturbance_${code}`;
          const shapeFeatureId = `disturbance_${code}_shape`;
          let feature = currentFeatures.find((ef) => ef.getId() === featureId);
          let featureShape = currentFeatures.find((ef) => ef.getId() === shapeFeatureId);
          if (!feature) {
            feature = new Feature({ ...disturbance });
            featureShape = new Feature({ ...disturbance });
            feature.setId(featureId);
            featureShape.setId(shapeFeatureId);
            this.push(feature, featureShape);
            featureShape.set('visibleMap', false);
          }
          const now = new Date(Date.now());
          const nowHeure = now.toISOString().substr(11, 8);
          const dateDebut = this.dateHelper.makeDate(disturbance.dateDebut);
          const dateFin = this.dateHelper.makeDate(disturbance.dateFin);
          const dansPeriode = this.dateHelper.isAfter(now, dateDebut) && this.dateHelper.isBefore(now, dateFin);
          const bWE = (
            (disturbance.weekEnd === '1' && (now.getDay() === 0 || now.getDay() === 6)) ||
            (disturbance.weekEnd === '0' && (now.getDay() > 0 && now.getDay() < 6)) ||
            disturbance.weekEnd === '2');
          const bHeure = (disturbance.heureDebut === '00:00:00' || disturbance.heureFin === '00:00:00' ||
            (disturbance.heureDebut > disturbance.heureFin && disturbance.heureDebut > nowHeure && nowHeure > disturbance.heureFin) ||
            (disturbance.heureDebut < disturbance.heureFin && !(nowHeure > disturbance.heureFin) && !(nowHeure < disturbance.heureDebut))
          );
          const enCours = dansPeriode && bWE && bHeure;
          const bientot = this.dateHelper.isAfter(this.dateHelper.addDays(now, 1), dateDebut) && !this.dateHelper.isAfter(now, dateDebut);
          const periode = { enCours, dansPeriode, bientot };
          feature.set('periode', periode);
          feature.set('visibleMap', periode.dansPeriode || periode.bientot);
          const geom = new Point(transform([disturbance.longitude, disturbance.latitude], this.dataProjection, this.featureProjection));
          feature.setGeometry(geom);
          feature.set('keep', true);
          if (disturbance.shape && featureShape) {
            // Event's shape
            const shapeGeometry = this.polyline.readGeometry(disturbance.shape, {
              dataProjection: this.dataProjection,
              featureProjection: this.featureProjection
            });
            featureShape.setGeometry(shapeGeometry);
            featureShape.set('type', 'event_shape');
            featureShape.set('keep', true);
          }
        }
        currentFeatures.forEach((feature) => {
          if (!feature.get('keep')) {
            this.remove(feature);
          }
        });
        return true;
      })
    );
  }

  update(): Observable<boolean> {
    return this.getData();
  }
}
