import {HttpClient, HttpParams} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {ConfigService, DataTypeCollection, FRONT_CONFIG, TypesHelper} from '@metromobilite/m-features/core';
import {MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase} from '@metromobilite/m-map';
import {Feature} from 'ol';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import {bbox} from 'ol/loadingstrategy';
import {transform, transformExtent} from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import {PointsResponse} from '../m-layers.model';


@Injectable({providedIn: 'root'})
export class MLayersPoiSource extends MapSourceBase {

  source: VectorSource;
  types: string[] = [];
  private compRes: any;
  private compRej: any;
  complete = new Promise((res, rej) => {
    this.compRes = res;
    this.compRej = rej;
  });

  constructor(
    private http: HttpClient,
    private frontConfig: ConfigService,
    private typesHelper: TypesHelper,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
  ) {
    super();
    // TODO: voir si possible de faire mieux niveau loader
    // trop de points charger en même temps, alors qu'ils sont probablement déjà la).
    this.source = new VectorSource({
      strategy: bbox,
      loader: async (extent, resolution, projection) => {
        if (this.types.length === 0) {
          const dataTypes: DataTypeCollection = await this.frontConfig.getData(FRONT_CONFIG.DATA_TYPES).toPromise();
          // Récupération des types de données depuis le dataTypes.json
          this.types = Object.entries(dataTypes).filter(([_, data]) => (data as any).map).map(([type, _]) => {
            // Ajoute le numéro de version s’il existe
            return _.version ? type + '@' + _.version : type;
          });
        }
        const tExtent = transformExtent(extent, projection, this.dataProjection);
        const params = new HttpParams({
          fromObject: {
            xmin: `${tExtent[0]}`.replace('Infinity', '180'),
            ymin: `${tExtent[1]}`.replace('Infinity', '180'),
            xmax: `${tExtent[2]}`.replace('Infinity', '180'),
            ymax: `${tExtent[3]}`.replace('Infinity', '90'),
            types: this.types.join(','),
            epci: 'All',
          },
        });

        this.http.get<PointsResponse>(`@domain/@api/points/json`, {params}).subscribe(response => {
          for (const responseFeature of response.features) {
            const type = responseFeature.properties.type;
            const id = `${type}_${(responseFeature.properties as any)[this.typesHelper.idField(type)]}`;
            let feature = this.source.getFeatureById(id);

            if (!feature) {
              const geometry = new Point(transform(responseFeature.geometry.coordinates, this.dataProjection, this.featureProjection));
              feature = new Feature({
                geometry,
                ...responseFeature.properties,
              });
              feature.setId(id);
              this.push(feature);
            }
          }
          this.compRes();
        });
      },
    });
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  isComplete(): Promise<unknown> {
    return this.complete;
  }
}
