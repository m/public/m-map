import {Injectable} from '@angular/core';
import {PonyService} from '@metromobilite/m-features/dyn';
import {MapSourceBase} from '@metromobilite/m-map';
import {Feature} from 'ol';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import {fromLonLat} from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class MLayersPonySource extends MapSourceBase {

  source = new VectorSource();

  constructor(private ponyService: PonyService) {
    super();
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    return this.ponyService.getData().pipe(map(response => {
      for (const bike of response.data.bikes) {
        const featureId = `ponyVehicle_${bike.bike_id}`;
        let feature = this.source.getFeatureById(featureId);
        const geometry = new Point(fromLonLat([bike.lon, bike.lat]));
        if (!feature) {
          feature = new Feature({
            ...bike, visibleMap: true, type: 'ponyVehicle',
          });
          feature.setGeometry(geometry);
          feature.setId(featureId);
          this.push(feature);
        } else {
          feature.setGeometry(geometry);
          feature.setProperties(bike);
        }
      }
      // Remove old features
      const currentIds = response.data.bikes.map(f => `ponyVehicle_${f.bike_id}`);
      const toRemove = this.source.getFeatures()
      .filter(i => !currentIds.includes(i.getId() as string));
      for (const feature of toRemove) {
        this.remove(feature);
      }
      return true;
    }), catchError(err => of(false)));
  }

  update(): Observable<boolean> {
    return this.getData();
  }

}
