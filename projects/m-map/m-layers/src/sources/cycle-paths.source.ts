import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import Polyline from 'ol/format/Polyline';
import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import VectorSource from 'ol/source/Vector';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { M_LAYERS_AVAILABLE_CYCLE_PATHS } from '../m-layers.token';
import { LinePolyResponse } from '../m-layers.model';

@Injectable({ providedIn: 'root' })
export class MLayersCyclePathsSource extends MapSourceBase {

  source: VectorSource;

  private polyline = new Polyline();

  constructor(
    private httpClient: HttpClient,
    @Inject(M_LAYERS_AVAILABLE_CYCLE_PATHS) public availableCyclePaths: string[],
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string
  ) {
    super();
    this.source = new VectorSource({});
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    if (this.source.getFeatures().length > 0) {
      return of(true);
    }
    return this.httpClient.get<LinePolyResponse>(`@domain/@api/lines/poly`, {
      params: new HttpParams({
        fromObject: {
          types: this.availableCyclePaths.join(','),
          epci: 'All'
        }
      })
    }).pipe(
      map(response => {
        for (const feature of response.features) {
          const geometry = this.getGeometry(feature.properties.shape);
          const f = new Feature({
            geometry,
            ...feature.properties
          });
          f.setId(feature.properties.ogc_fid);
          this.push(f);
        }
        return true;
      }),
      catchError(err => of(false))
    );
  }

  private getGeometry(shape: string | string[]): SimpleGeometry {
    if (Array.isArray(shape)) {
      return new MultiLineString(shape.map(s => {
        return this.getGeometry(s) as LineString;
      }));
    } else {
      return this.polyline.readGeometry(shape, {
        dataProjection: this.dataProjection,
        featureProjection: this.featureProjection
      }) as LineString;
    }
  }

}
