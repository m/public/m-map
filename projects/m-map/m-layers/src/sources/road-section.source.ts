import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { RoadSectionService, Section } from '@metromobilite/m-features/dyn';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Polyline from 'ol/format/Polyline';
import Geometry from 'ol/geom/Geometry';
import LineString from 'ol/geom/LineString';
import VectorSource from 'ol/source/Vector';
import { of } from 'rxjs';
import { Observable, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MLayersRoadSectionSource extends MapSourceBase {

  source: VectorSource;

  private polyline = new Polyline();

  constructor(
    private http: HttpClient,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private roadSectionService: RoadSectionService
  ) {
    super();
    this.source = new VectorSource({});
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    return zip(
      this.http.get<{ features: { properties: { id: string, shape: string } }[] }>(`@domain/@api/lines/poly?types=trr`),
      this.roadSectionService.getData()
    ).pipe(
      map(([response, dynResponse]) => {
        for (const feature of response.features) {
          const geometry = this.polyline.readGeometry(feature.properties.shape, {
            dataProjection: this.dataProjection,
            featureProjection: this.featureProjection
          }) as LineString;
          delete feature.properties.shape;
          
          const dynData: Section[] = dynResponse[feature.properties.id] as any;
          const f = new Feature({
            geometry,
            ...feature.properties,
            nsv: dynData && dynData[dynData.length - 1].nsv_id || 0,
          });
          f.setId(feature.properties.id);
          this.push(f);
        }
        return true;
      }),
      catchError(err => of(false))
    );
  }

  update(): Observable<boolean> {
    return this.roadSectionService.getData().pipe(
      map(response => {
        this.source.getFeatures().forEach(feature => {
          const id = feature.getId();
          if (id && response[id]) {
            const data: Section[] = response[id] as any;
            feature.set('nsv', data[data.length - 1].nsv_id);
          }
        });
        return true;
      }),
      catchError(err => of(false))
    );
  }

}
