import {Inject, Injectable, OnDestroy} from '@angular/core';
import {LetaxiService} from '@metromobilite/m-features/core';
import {
  GeolocationSource,
  MAP_PROJECTION_DESTINATION,
  MAP_PROJECTION_SOURCE,
  MapSourceBase,
} from '@metromobilite/m-map';
import {Feature} from 'ol';
import {Coordinate} from 'ol/coordinate';
import {Circle as CircleGeom, Geometry, Point} from 'ol/geom';
import {fromLonLat, getPointResolution, transform} from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import {Circle, Fill, Stroke, Style} from 'ol/style';
import {Observable, of, Subject} from 'rxjs';
import {catchError, map, takeUntil} from 'rxjs/operators';
import {LetaxiMapService} from '../services/letaxiMap.service';
import {MLayersPoiSource} from './poi.source';
import {LeTaxiModels, LeTaxiProperties} from '@metromobilite/m-features/core/src/letaxi/letaxi.model';

@Injectable({providedIn: 'root'})
export class MLayersLetaxiSource extends MapSourceBase implements OnDestroy {

  source: VectorSource;
  zoneSource: VectorSource<any> = new VectorSource();
  // Coordinates of user position in EPSG:3857: [637081.4458099046, 5651617.593818558]
  private coordinates: Coordinate;
  // Coordinates of user position in number[]: [2.3522219000000177, 48.85661400000001]
  private position: number[];
  private unSubscriber = new Subject();
  // Circle 500m around user
  private zoneCircle: Feature<Geometry>;
  // Circle user
  private featurePos: Feature<Geometry> = new Feature();
  private readonly LETAXI_POS = 'letaxiPos';
  private readonly LETAXI_ZONE = 'letaxiZone';
  public taxiSelected: string = null;

  constructor(private letaxiService: LetaxiService,
              private geolocationSource: GeolocationSource,
              private letaxiMapService: LetaxiMapService,
              @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
              @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
              source: MLayersPoiSource) {
    super();

    this.source = source.getSource() as VectorSource;
    // Get user or adresse position
    this.letaxiMapService.initData()
    .pipe(takeUntil(this.unSubscriber))
    .subscribe((res: Coordinate) => {
      this.coordinates = res;
      this.position = this.convertToGeo(res);
      this.generateFeatures(this.coordinates);
    });

  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getData(): Observable<boolean> {
    if (!this.position) {
      return of(false);
    }
    // Get data from letaxi service
    return this.letaxiService.getData(this.position).pipe(
      map((res: LeTaxiModels) => {
        this.handleResponse(res);
        return true;
      }),
      catchError(() => of(false)));
  }

  update(): Observable<boolean> {
    return this.getData();
  }

  ngOnDestroy(): void {
    this.unSubscriber.next();
    this.unSubscriber.complete();
  }

  /**
   * Convert coordinates from EPSG:3857 to data projection
   * @param geo coordinates in EPSG:3857
   * @returns coordinates in data projection
   */
  public convertToGeo(geo: Coordinate): Coordinate {
    return transform(geo, this.featureProjection, this.dataProjection);
  }

  /**
   * Create feature: double circle around position
   * @param geo user position in EPSG
   */
  private generateFeatures(geo: Coordinate): void {
    if (!geo) {
      return;
    }
    this.createAdressePosition(geo);
    this.createCircleFeature(geo);
  }

  /**
   * Create feature position adresse
   * @param geo user position in EPSG
   */
  private createAdressePosition(geo: Coordinate): void {
    if (!this.zoneSource.getFeatureById(this.LETAXI_POS)) {
      this.featurePos.setStyle(new Style({
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: 'rgba(108,37,115,1)',
          }),
          stroke: new Stroke({
            color: '#fff',
            width: 2,
          }),
        }),
      }));
      this.featurePos.setId(this.LETAXI_POS);
      this.featurePos.setGeometry(new Point(geo));
      this.zoneSource.addFeature(this.featurePos);
    } else {
      this.featurePos.setGeometry(new Point(geo));
    }
  }

  /**
   * Create a circle feature around the user position
   * @param geo user position in EPSG
   */
  private createCircleFeature(geo: Coordinate): void {
    // If circle feature already exist, remove it
    if (this.zoneSource && this.zoneSource.getFeatureById(this.LETAXI_ZONE)) {
      this.zoneSource.removeFeature(this.zoneSource.getFeatureById(this.LETAXI_ZONE));
    }
    // Else create it
    this.zoneCircle = new Feature(new CircleGeom(geo, 500 / getPointResolution(this.featureProjection, 1, geo, 'm')));
    // Set feature ID
    this.zoneCircle.setId(this.LETAXI_ZONE);
    // Add feature to source
    this.zoneSource.addFeature(this.zoneCircle);
  }

  /**
   * Dispatch response to features
   * @param response response from letaxi service
   */
  private handleResponse(response: any): void {
    if (response && response.features && response.features.length > 0) {

      response.features.forEach((taxiFeature: any) => {
        const type: string = taxiFeature.properties.type;
        const id = `${type}_${taxiFeature.properties.id}`;
        const geometry: Point = new Point(fromLonLat(taxiFeature.geometry.coordinates));
        const f: Feature<Geometry> = this.source.getFeatureById(id);
        const property: LeTaxiProperties = taxiFeature.properties;
        // If feature doesn't exist, create it
        if (!f) {
          // create feature
          const featurePins: Feature<Geometry> = new Feature({
            geometry,
            visibleMap: true,
            label: 'Taxi',
            type,
            ...property,
          });
          // set feature id
          featurePins.setId(id);
          featurePins.set('activeTaxi', false);
          // add feature to source
          this.push(featurePins);
        } else {
          // Else update it
          f.setGeometry(geometry);
          f.setProperties({...f.getProperties(), ...property});
        }
      });

      if (this.taxiSelected) {
        this.keepTaxiOnMap(this.taxiSelected, true);
      }

    }

    // Remove features that are no longer in the zone
    const currentIds = response.features.map((a: any) => `${a.properties.type}_${a.properties.id}`);
    const toRemove = this.source.getFeatures()
    .filter((f: Feature<Geometry>): boolean => f.get('type') === 'letaxi')
    .map((f: Feature<Geometry>) => {
      return f.get('activeTaxi') ? null : f.getId();
    })
    .filter((id: string) => {
      return id ? !currentIds.includes(id) : false;
    });

    for (const id of toRemove) {
      this.remove(this.source.getFeatureById(id));
    }

  }

  /**
   * Select or unselect taxi feature
   * @param id feature id
   * @param value hide or show feature
   * @return void
   */
  public keepTaxiOnMap(id: string, value: boolean): void {
    if (this.source && this.source.getFeatures()) {
      this.source.getFeatures().forEach((f: Feature<Geometry>) => {
        if (f && f.getId() === id) {
          this.taxiSelected = id;
          f.set('activeTaxi', value);
          f.set('visible', true);
        } else {
          f.set('visible', false);
        }
      });
    }
  }

  /**
   * Can be used to remove all taxi features in map
   * @return void
   */
  public leaveTaxi(): void {
    if (this.source.getFeatures().length > 0) {
      this.taxiSelected = null;
      // Remove all taxi features
      this.source.getFeatures().forEach((f: Feature<Geometry>) => {
        if (f.get('type') === 'letaxi') {
          // Enabled taxi
          f.set('visible', true);
          // Reset property
          f.set('activeTaxi', false);
        }
      });
    }
  }

  /**
   * Can be used to update feature properties and move it
   * @param id feature id
   * @param prop feature properties
   * @return void
   */
  public updateFeature(id: string, prop: LeTaxiProperties): void {
    // Check if feature exist
    if (this.source && this.source.getFeatureById(id)) {
      // Regroup coordinates
      const coordinate: number[] = [prop.position.lon, prop.position.lat];
      if (coordinate[0] && coordinate[1]) {
        // Update feature geometry
        this.source.getFeatureById(id).setGeometry(new Point(fromLonLat(coordinate)));
      }
      // Update feature properties
      this.source.getFeatureById(id).setProperties(prop);
    }
  }

}
