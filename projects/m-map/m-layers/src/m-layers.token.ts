import { InjectionToken } from '@angular/core';

export const M_LAYERS_AVAILABLE_LINE_TYPES = new InjectionToken<string[]>('MLayersAvailableLineTypes');
export const M_LAYERS_AVAILABLE_CYCLE_PATHS = new InjectionToken<string[]>('MLayersAvailableCyclePaths');

export const M_LAYERS_LAYER_HELPER = new InjectionToken('MLayersLayerHelper');
