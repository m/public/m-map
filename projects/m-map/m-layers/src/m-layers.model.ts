import { MapFilter } from '@metromobilite/m-features/core';
import { LayerInputConfig, MapComponent, MapSourceInterface } from '@metromobilite/m-map';
import { Feature } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { Extent } from 'ol/extent';
import Geometry from 'ol/geom/Geometry';
import { Observable } from 'rxjs';

export interface LineStyleConfig {
  color: string | number[];
  alternateColor?: string | number[];
  backgroundColor?: string | number[];
  width?: number;
  zOffset?: number;
  lineDash?: [number, number];
  lineDashOffset?: number;
  noBorder?: boolean;
}

export interface PlygonStyleConfig {
  color: string | number[];
  alternateColor?: string | number[];
  strokeColor?: string | number[];
  width?: number;
  zOffset?: number;
  lineDash?: [number, number];
}

export interface IconStyleConfig {
  icon: {
    src: string;
    rotation?: number;
    opacity?: number;
    scale?: number;
    anchor?: [number, number]
  };
  zIndex: number;
}

export interface CircleStyleConfig {
  color: string | number[];
  radius: number;
  strokeColor: string | number[];
  strokeWidth: number;
  zIndex: number;
}

export interface TextStyleConfig {
  zIndex?: number;
  font?: string;
  text: string;
  textAlign?: CanvasTextAlign;
  textBaseline?: CanvasTextBaseline;
  offsetX?: number;
  offsetY?: number;
  fillColor?: string | number[];
  strokeColor?: string | number[];
  strokeWidth?: number;
}

export interface PointsResponse {
  features: {
    geometry: {
      coordinates: Coordinate
    },
    properties: {
      type: string,
      id: string,
      visible?: boolean,
    }
  }[];
}

export interface LinePolyResponse {
  features: {
    properties: {
      ogc_fid: string,
      shape: string[] | string
    }
  }[];
}

export interface LayersConfig {
  [key: string]: LayerConfig;
}

export interface LayerConfig {
  weight: number;
  unique: boolean;
  layer?: string;
  build?: (config: MapFilter) => LayerInputConfig;
}

export interface LayerHelperConfig {
  weight: number;
  unique: boolean;
  layer: string;
  id: string;
}

export enum LAYER_HELPER_META {
  CONFIG = 'm-layers:layer-helper:config',
}

export interface LayerHelperBuild {

  build(config: MapFilter): LayerInputConfig;

}

export interface LayerHelperStateChange {

  /**
   * Handle the map filter state changes.
   * Should retrun false or an observable.
   * Observable<Observable<boolean>> === update.
   */
  stateChange(event: StateChangeEvent): Observable<boolean | Observable<boolean>>;
}

export interface LayerHelperSortLayers {
  sortLayers(layers: LayerInputConfig[], leafs: MapFilter[]): void;
}

export interface LayerHelperVisibleFeatures {
  getVisibleFeatures(layer: LayerInputConfig, extent: Extent, resolution: number): Feature<Geometry>[];
}

export interface LayerHelperSource {
  getSource(): MapSourceInterface;
}

export type StateChangeEventType = 'add' | 'remove';

export type LineToAddMode = 'type' | 'line';

export interface StateChangeEvent {
  helperId: string;
  event: StateChangeEventType;
  data: MapFilter[];
  layers: LayerInputConfig[];
  map: MapComponent;
  others: MapFilter[];
}


export interface StateConfig {
  [key: string]: string[];
}
