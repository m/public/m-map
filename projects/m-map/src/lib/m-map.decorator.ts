import 'reflect-metadata';
import { MAP_BEHAVIOR_META, MAP_LAYER_META } from './m-map.model';

export function MapBehavior(id: string): (target: any) => void {
  return (target: any) => {
    Reflect.defineMetadata(MAP_BEHAVIOR_META.ID, id, target);
  };
}

export function MapLayer(id: string): (target: any) => void {
  return (target: any) => {
    Reflect.defineMetadata(MAP_LAYER_META.ID, id, target);
  };
}
