import { SimpleChanges } from '@angular/core';
import { Feature } from 'ol';
import BaseEvent from 'ol/events/Event';
import BaseLayer from 'ol/layer/Base';
import Source from 'ol/source/Source';
import { Observable } from 'rxjs';
import { MapComponent } from './m-map.component';

export enum MAP_BEHAVIOR_META {
  ID = 'm-map:behavior:id',
}

export enum MAP_LAYER_META {
  ID = 'm-map:layer:id',
}

export interface MapInit {
  onMapInit(component: MapComponent): void;
}

export interface MapDestroy {
  onMapDestroy(component: MapComponent): void;
}

export interface AfterMapInit {
  onAfterMapInit(component: MapComponent): void;
}

export interface MapInputPropertiesChanges {
  onMapInputPropertiesChanges(changes: SimpleChanges, component: MapComponent): void;
}

export interface MapSourceInterface {

  getSource(): Source;

  push(...features: Feature[]): void;

  remove(feature: Feature): void;

  find(id: string): Feature | null;

  /**
   * Call the openlayer source refresh method.
   */
  refresh(): void;

  /**
   * Call the openlayer source changed method.
   */
  changed(): void;

  /**
   * Listen to an Openlayers source event.
   */
  on(eventType: SourceEventTypes): Observable<BaseEvent>;

  /**
   * Complete the rxjs subject and remove the listener from the Openlayers source.
   */
  un(eventType: SourceEventTypes): void;

}

export interface MapLayerInterface {

  source: MapSourceInterface;

  /**
   * Responsible to create a layer instance.
   */
  buildLayer(component: MapComponent, config: LayerInputConfig): BaseLayer;

  /**
   * Set the visibility property to true for the given layer in the given map.
   * Also update the layer input config (tiles/layers input properties).
   */
  show(component: MapComponent, id: string | number): void;

  /**
   * Set the visibility property to false for the given layer in the given map.
   * Also update the layer input config (tiles/layers input properties).
   */
  hide(component: MapComponent, id: string | number): void;

}

export interface LayerInputConfig {
  layer: string;
  visible: boolean;
  /**
   * @internal
   * The layer uid from openlayers getUid() function.
   * Set during the add to map process.
   */
  _uid?: string | number;
  [key: string]: any;
}

export type SourceEventTypes = 'addfeature' | 'change' | 'changefeature' | 'clear' | 'error' | 'featuresloadend' | 'featuresloaderror' | 'featuresloadstart' | 'propertychange' | 'removefeature';
