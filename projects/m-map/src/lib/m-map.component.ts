import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import MapEvent from 'ol/events/Event';
import { Coordinate } from 'ol/coordinate';
import { MapBehaviorsService } from './m-map-behaviors.service';
import { LayerInputConfig } from './m-map.model';

@Component({
  selector: 'm-map',
  template: `
    <div #map class="map">
      <div class="map-center" *ngIf="displayCenter"></div>
      <div class="ol-unselectable map-controls">
        <div *ngIf="enableZoomControls" class="zoomMap">
          <button mat-mini-fab color="accent" class="ol-zoom-in" (click)="zoomIn()">
              <mat-icon>add</mat-icon>
          </button>
          <button mat-mini-fab color="accent" class="ol-zoom-out" (click)="zoomOut()">
            <mat-icon>remove</mat-icon>
          </button>
        </div>
      </div>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./m-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit, OnChanges, OnDestroy {

  @Input() coordinates: Coordinate;
  // = [5.724681, 45.189053];
  @Input() zoom = 12;
  @Input() maxZoom = 18;
  @Input() minZoom = 12;
  @Input() displayCenter = true;
  @Input() enableZoomControls = true;
  /**
   * This property defines the layers to be used as a tile layer, it is a functional split of layer.
   */
  @Input() tiles: LayerInputConfig[] = [{ layer: 'core:xyz-dark', visible: true }];
  /**
   * This property defines the layers to display.
   * You can add tile layers to the map with this property but prefer the use of the tiles property instead.
   */
  @Input() layers: LayerInputConfig[] = [];
  @Input() behaviors = ['core:init', 'core:map-events', 'core:after-init', 'core:animated-zoom'];

  @Output() mapChange = new EventEmitter<MapEvent>();
  @Output() mapClick = new EventEmitter<MapEvent>();
  @Output() mapDblClick = new EventEmitter<MapEvent>();
  @Output() mapError = new EventEmitter<MapEvent>();
  @Output() mapMoveEnd = new EventEmitter<MapEvent>();
  @Output() mapMoveStart = new EventEmitter<MapEvent>();
  @Output() mapPointerDrag = new EventEmitter<MapEvent>();
  @Output() mapPointerMove = new EventEmitter<MapEvent>();
  @Output() mapPostCompose = new EventEmitter<MapEvent>();
  @Output() mapPostRender = new EventEmitter<MapEvent>();
  @Output() mapPreCompose = new EventEmitter<MapEvent>();
  @Output() mapPropertyChange = new EventEmitter<MapEvent>();
  @Output() mapRenderComplete = new EventEmitter<MapEvent>();
  @Output() mapSingleClick = new EventEmitter<MapEvent>();

  /**
   * Special event used by a map behavior service that need to emit a non core event.
   */
  @Output() behaviorEvent = new EventEmitter<any>();

  @ViewChild('map', { static: true }) public mapContainer!: ElementRef<HTMLElement>;

  public map!: Map;
  public view!: View;

  constructor(
    private mapBehaviorsService: MapBehaviorsService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {    
    if (this.map) {
      for (const id of this.behaviors) {
        if ('onMapInputPropertiesChanges' in this.mapBehaviorsService.behaviors[id]) {
          this.mapBehaviorsService.behaviors[id].onMapInputPropertiesChanges(changes, this);
        }
      }
    }
  }

  ngOnInit(): void {   
    // Call init behaviors.
    for (const id of this.behaviors) {
      if ('onMapInit' in this.mapBehaviorsService.behaviors[id]) {
        this.mapBehaviorsService.behaviors[id].onMapInit(this);
      }
    }
    // Call after init behaviors.
    for (const id of this.behaviors) {
      if ('onAfterMapInit' in this.mapBehaviorsService.behaviors[id]) {
        this.mapBehaviorsService.behaviors[id].onAfterMapInit(this);
      }
    }
  }

  ngOnDestroy(): void {
    for (const id of this.behaviors) {
      if ('onMapDestroy' in this.mapBehaviorsService.behaviors[id]) {
        this.mapBehaviorsService.behaviors[id].onMapDestroy(this);
      }
    }
  }

  zoomIn(){
		this.view.animate({
			zoom: this.view.getZoom() + 1,
			duration: 200
		});
	}

	zoomOut(){
		this.view.animate({
			zoom: this.view.getZoom() - 1,
			duration: 200
		});
	}

}
