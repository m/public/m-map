import { InjectionToken } from '@angular/core';

export const MAP_BEHAVIOR = new InjectionToken('MapBehavior');
export const MAP_LAYER = new InjectionToken('MapLayer');
export const MAP_SOURCE = new InjectionToken('MapSource');

export const MAP_XYZ_LIGHT_URL = new InjectionToken<string>('MapXYZLightUrl');
export const MAP_XYZ_DARK_URL = new InjectionToken<string>('MapXYZDarkUrl');
export const MAP_MAX_EXTENT = new InjectionToken<[number, number, number]>('MapMaxExtent');

export const MAP_PROJECTION_SOURCE = new InjectionToken<string>('MapProjectionSource');
export const MAP_PROJECTION_DESTINATION = new InjectionToken<string>('MapProjectionDestination');
