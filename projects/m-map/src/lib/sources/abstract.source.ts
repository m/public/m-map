import { Feature } from 'ol';
import BaseEvent from 'ol/events/Event';
import Geometry from 'ol/geom/Geometry';
import Source from 'ol/source/Source';
import { Observable, Subject } from 'rxjs';
import { MapSourceInterface, SourceEventTypes } from '../m-map.model';

export abstract class MapSourceBase implements MapSourceInterface {

  protected source!: Source;
  protected events: { [key in SourceEventTypes]?: { subject: Subject<BaseEvent>, listener: (p0: any) => any } } = {};

  getSource(): Source {
    return this.source;
  }

  refresh(): void {
    this.source.refresh();
  }

  changed(): void {
    this.source.changed();
  }

  on(eventType: SourceEventTypes): Observable<BaseEvent> {
    if (!this.events[eventType]) {
      this.events[eventType] = {
        subject: new Subject(),
        listener: (event => {
          this.events[eventType]?.subject.next(event);
        })
      };
      /* tslint:disable-next-line:no-non-null-assertion */
      this.source.on((eventType as any), this.events[eventType]!.listener);
    }
    /* tslint:disable-next-line:no-non-null-assertion */
    return this.events[eventType]!.subject.asObservable();
  }

  un(eventType: SourceEventTypes): void {
    if (this.events[eventType]) {
      this.events[eventType]?.subject.complete();
      /* tslint:disable-next-line:no-non-null-assertion */
      this.source.un((eventType as any), this.events[eventType]!.listener);
    }
  }

  abstract push(...features: Feature<Geometry>[]): void;

  abstract remove(feature: Feature<Geometry>): void;

  abstract find(id: string): Feature<Geometry> | null;

}
