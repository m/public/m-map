import {Inject, Injectable} from '@angular/core';
import {Feature, Geolocation} from 'ol';
import {Coordinate} from 'ol/coordinate';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector';
import Circle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import {BehaviorSubject, Subject} from 'rxjs';
import {MAP_PROJECTION_DESTINATION} from '../m-map.token';
import {MapSourceBase} from './abstract.source';

@Injectable({providedIn: 'root'})
export class GeolocationSource extends MapSourceBase {

  source: VectorSource;
  geolocation: Geolocation;
  accuracyFeature: Feature<Geometry>;
  positionFeature: Feature<Geometry>;

  position$ = new Subject<Coordinate>();
  enable$ = new BehaviorSubject<boolean | undefined>(undefined);
  enabled = false;
  displayAccuracy = true;

  constructor(
    @Inject(MAP_PROJECTION_DESTINATION) public projDestination: string,
  ) {
    super();
    this.accuracyFeature = new Feature();
    this.positionFeature = new Feature();
    this.positionFeature.setStyle(new Style({
      image: new Circle({
        radius: 7,
        fill: new Fill({
          color: '#0071BC',
        }),
        stroke: new Stroke({
          color: '#fff',
          width: 2,
        }),
      }),
    }));

    this.source = new VectorSource({
      features: [this.accuracyFeature, this.positionFeature],
    });

    this.geolocation = new Geolocation({
      trackingOptions: {
        enableHighAccuracy: true,
        maximumAge: 0,
        timeout: 5000,
      },
      projection: this.projDestination,
    });

    this.geolocation.on('change:position', () => {
      const coordinates = this.geolocation.getPosition();
      this.positionFeature.setGeometry(coordinates ? new Point(coordinates) : undefined);
      this.position$.next(coordinates);
      if (!this.enabled) {
        this.enable$.next(true);
        this.enabled = true;
      }
    });

    this.geolocation.on('change:accuracyGeometry', () => {
      if (this.displayAccuracy) {
        this.accuracyFeature.setGeometry(this.geolocation.getAccuracyGeometry());
      }
    });

    this.geolocation.on('error', () => {
      this.enable$.next(false);
    });
  }

  hideAccuracyFeature(): void {
    this.displayAccuracy = false;
    this.accuracyFeature.setGeometry(undefined);
  }

  showAccuracyFeature(): void {
    this.displayAccuracy = true;
    this.accuracyFeature.setGeometry(this.geolocation.getAccuracyGeometry());
  }

  enable(): void {
    this.geolocation.setTracking(true);
  }

  disable(): void {
    this.geolocation.setTracking(false);
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    throw new Error('Method not implemented.');
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    throw new Error('Method not implemented.');
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    throw new Error('Method not implemented.');
  }

}
