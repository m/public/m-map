import { Inject, Injectable } from '@angular/core';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import XYZ from 'ol/source/XYZ';
import { MAP_XYZ_DARK_URL } from '../m-map.token';
import { MapSourceBase } from './abstract.source';
import { Source } from 'ol/source';

@Injectable({ providedIn: 'root' })
export class XYZDynamicSource extends MapSourceBase {

  protected source: XYZ;

  constructor() {
    super();
    this.source = new XYZ({
      url: 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attributions: [
        /* tslint:disable:max-line-length */
        'Tiles courtesy of <a href="https://www.jawg.io" target="_blank" rel="noopener">jawgmaps</a> - Map data <a href="https://www.openstreetmap.org/copyright" target="_blank" rel="noopener">©OpenStreetMap contributors</a>, under ODbL.'
        /* tslint:enable */
      ],
    });
  }

  updateSource(url : string) {
    this.source.setUrl(url)
  }

  push(...features: Feature<Geometry>[]): void {
    throw new Error('Method not available on this source.');
  }

  remove(feature: Feature<Geometry>): void {
    throw new Error('Method not available on this source.');
  }

  find(id: string): Feature<Geometry> | null {
    throw new Error('Method not available on this source.');
  }

}
