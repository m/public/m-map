import Style from 'ol/style/Style';

export abstract class AbstractStyle {

  protected cache: { [key: string]: Style | Style[] } = {};

  build(...args: any[]): Style | Style[] {
    const hash = JSON.stringify(args);
    if (this.cache[hash]) {
      return this.cache[hash];
    } else {
      const style = this.makeStyle(...args);
      this.cache[hash] = style;
      return style;
    }
  }

  protected abstract makeStyle(...args: any[]): Style | Style[];

}
