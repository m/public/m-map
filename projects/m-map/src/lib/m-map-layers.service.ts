import {Inject, Injectable} from '@angular/core';
import {MapComponent} from './m-map.component';
import {MAP_LAYER_META, MapLayerInterface} from './m-map.model';
import {MAP_LAYER} from './m-map.token';

@Injectable({providedIn: 'root'})
export class MapLayersService {
  layers: { [key: string]: MapLayerInterface };

  constructor(@Inject(MAP_LAYER) layers: MapLayerInterface[]) {
    this.layers = layers.reduce((acc, layer) => {
      const name = Reflect.getMetadata(MAP_LAYER_META.ID, layer.constructor);
      acc[name] = layer;
      return acc;
    }, {} as { [key: string]: MapLayerInterface });    
  }

  /**
   * Set the visibility property to true for the given layer in the given map.
   * Also update the layer input config (tiles/layers input properties).
   */
  show(component: MapComponent, id: string | number): void {
    const config = [...component.tiles, ...component.layers].find(c => c._uid === id);
    if (config) {
      this.layers[config.layer].show(component, id);
    }
  }

  /**
   * Set the visibility property to false for the given layer in the given map.
   * Also update the layer input config (tiles/layers input properties).
   */
  hide(component: MapComponent, id: string | number): void {
    const config = [...component.tiles, ...component.layers].find(c => c._uid === id);
    if (config) {
      this.layers[config.layer].hide(component, id);
    }
  }

}
