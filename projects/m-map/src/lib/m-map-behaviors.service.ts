import { Inject, Injectable } from '@angular/core';
import { MAP_BEHAVIOR_META } from './m-map.model';
import { MAP_BEHAVIOR } from './m-map.token';

@Injectable({ providedIn: 'root' })
export class MapBehaviorsService {
  behaviors: { [key: string]: any };

  constructor(@Inject(MAP_BEHAVIOR) availableBehaviors: any[]) {
    this.behaviors = availableBehaviors.reduce((acc, behavior) => {
      const name = Reflect.getMetadata(MAP_BEHAVIOR_META.ID, behavior.constructor);
      acc[name] = behavior;
      return acc;
    }, {});
  }

}
