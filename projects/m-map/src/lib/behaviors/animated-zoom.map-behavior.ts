import { Injectable, SimpleChanges } from '@angular/core';
import { MapComponent } from '../m-map.component';
import { MapBehavior } from '../m-map.decorator';
import { MapInputPropertiesChanges } from '../m-map.model';

@Injectable({ providedIn: 'root' })
@MapBehavior('core:animated-zoom')
export class AnimatedZoom implements MapInputPropertiesChanges {

  onMapInputPropertiesChanges(changes: SimpleChanges, component: MapComponent): void {
    if (changes.zoom) {
      component.view.animate({ zoom: changes.zoom.currentValue, duration: 300 }, complete => {
        component.behaviorEvent.emit({ type: 'animatedZoom', data: complete });
      });
    }
  }

}
