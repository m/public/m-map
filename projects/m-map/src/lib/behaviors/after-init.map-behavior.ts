import { Injectable } from '@angular/core';
import { MapComponent } from '../m-map.component';
import { MapBehavior } from '../m-map.decorator';
import { AfterMapInit } from '../m-map.model';

@Injectable({ providedIn: 'root' })
@MapBehavior('core:after-init')
export class AfterInit implements AfterMapInit {

  onAfterMapInit(component: MapComponent): void {
    setTimeout(() => {
      // Fix wrong canvas width on route change.
      component.map.updateSize();
    });
  }

}
