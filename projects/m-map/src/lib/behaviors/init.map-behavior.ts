import { Inject, Injectable, SimpleChange, SimpleChanges } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import * as proj from 'ol/proj';
import { getUid } from 'ol/util';
import { MapComponent } from '../m-map.component';
import { MapBehavior } from '../m-map.decorator';
import { LayerInputConfig, MapInit, MapInputPropertiesChanges, MAP_LAYER_META, MapDestroy } from '../m-map.model';
import { MapLayersService } from '../m-map-layers.service';
import BaseLayer from 'ol/layer/Base';
import { MAP_MAX_EXTENT, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '../m-map.token';
import { Extent } from 'ol/extent';
import { Attribution, defaults as defaultControls } from 'ol/control.js';

import { BreakpointObserver } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

@Injectable({ providedIn: 'root' })
@MapBehavior('core:init')
export class InitMap implements MapInit, MapInputPropertiesChanges, MapDestroy {
  subscription: Subscription;

  constructor(
    private layersService: MapLayersService,
    @Inject(MAP_PROJECTION_SOURCE) public projSource: string,
    @Inject(MAP_PROJECTION_DESTINATION) public projDestination: string,
    @Inject(MAP_MAX_EXTENT) public maxExtent: Extent,
    private breakpointObserver: BreakpointObserver
  ) {

  }

  onMapInit(component: MapComponent): void {
    const attribution = new Attribution({
      collapsible: true,
      collapsed: true,
    });

    const layers: BaseLayer[] = [];
    for (const config of component.tiles) {
      const layer = this.layersService.layers[config.layer].buildLayer(component, config);
      layer.set(MAP_LAYER_META.ID, config.layer);
      layer.setZIndex(component.tiles.indexOf(config));
      layer.setVisible(config.visible);
      config._uid = getUid(layer);
      layers.push(layer);
    }
    for (const config of component.layers) {
      const layer = this.layersService.layers[config.layer].buildLayer(component, config);
      layer.set(MAP_LAYER_META.ID, config.layer);
      layer.setZIndex(component.layers.indexOf(config) + component.tiles.length);
      layer.setVisible(config.visible);
      config._uid = getUid(layer);
      layers.push(layer);
    }

    component.view = new View({
      center: proj.transform(component.coordinates, this.projSource, this.projDestination),
      zoom: component.zoom,
      maxZoom: component.maxZoom,
      minZoom: component.minZoom,
      extent: proj.transformExtent(this.maxExtent, this.projSource, this.projDestination),
      enableRotation: false
    });
    component.map = new Map({
      controls: defaultControls({ zoom: false, attribution: false }).extend([attribution]),
      target: component.mapContainer.nativeElement,
      view: component.view,
      layers,
    });

    this.subscription = this.breakpointObserver.observe(['(max-width: 1080px)']).subscribe(results => {
      Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.height = 'fit-content';
      Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.right = '8px';
      Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.top = 'unset';
      Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.left = 'unset';
      

      if (results.matches) {
        Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.top = '56px';
        Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.height = 'fit-content';
        Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.right = 'unset';
        Array.from(document.getElementsByClassName('ol-attribution') as HTMLCollectionOf<HTMLElement>)[0].style.left = '8px';
      }
    });

  }

  onMapInputPropertiesChanges(changes: SimpleChanges, component: MapComponent): void {
    if (changes.tiles) {
      this.updateLayers(changes.tiles, component);
    }
    if (changes.layers) {
      this.updateLayers(changes.layers, component, component.tiles.length);
    }
    if (changes.maxZoom) {
      component.view.setMaxZoom(changes.maxZoom.currentValue);
    }
    if (changes.minZoom) {
      component.view.setMinZoom(changes.minZoom.currentValue);
    }
  }

  private updateLayers(changes: SimpleChange, component: MapComponent, zOffset = 0): void {
    const currentLayers = component.map.getLayers();
    // Remove old layers or update zIndex/visibility if the layer is still needed.
    currentLayers.forEach(layer => {
      const luid = getUid(layer);
      if (
        changes.previousValue.find((c: LayerInputConfig) => c._uid === luid) &&
        !changes.currentValue.find((c: LayerInputConfig) => c._uid === luid)
      ) {
        component.map.removeLayer(layer);
      } else {
        const index = changes.currentValue.findIndex((c: LayerInputConfig) => c._uid === luid);
        if (index > -1) {
          const config = changes.currentValue[index];
          layer.setVisible(config.visible);
          layer.setZIndex(index + zOffset);
        }
      }
    });
    // Add new layer and handle z index and visibility.
    const layers = component.map.getLayers();
    (changes.currentValue as LayerInputConfig[]).forEach((config, index) => {
      const luid = config._uid;
      if (!changes.previousValue.find((c: LayerInputConfig) => c._uid === luid)) {
        const layer = this.layersService.layers[config.layer].buildLayer(component, config);
        layer.set(MAP_LAYER_META.ID, config.layer);
        layer.setZIndex(index + zOffset);
        layer.setVisible(typeof config === 'string' ? true : config.visible);
        config._uid = getUid(layer);
        layers.push(layer);
      }
    });
  }

  onMapDestroy(component: MapComponent): void {
    this.subscription.unsubscribe();
  }

}
