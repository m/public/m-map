import { Injectable } from '@angular/core';
import { MapComponent } from '../m-map.component';
import { MapBehavior } from '../m-map.decorator';
import { AfterMapInit } from '../m-map.model';

@Injectable({ providedIn: 'root' })
@MapBehavior('core:map-events')
export class MapEvents implements AfterMapInit {

  onAfterMapInit(component: MapComponent): void {
    component.map.on('change', (evt) => component.mapChange.emit(evt));
    component.map.on('click', (evt) => component.mapClick.emit(evt));
    component.map.on('dblclick', (evt) => component.mapDblClick.emit(evt));
    component.map.on('error', (evt) => component.mapError.emit(evt));
    component.map.on('moveend', (evt) => component.mapMoveEnd.emit(evt));
    component.map.on('movestart', (evt) => component.mapMoveStart.emit(evt));
    component.map.on('pointerdrag', (evt) => component.mapPointerDrag.emit(evt));
    component.map.on('pointermove', (evt) => component.mapPointerMove.emit(evt));
    component.map.on('postcompose', (evt) => component.mapPostCompose.emit(evt));
    component.map.on('postrender', (evt) => component.mapPostRender.emit(evt));
    component.map.on('precompose', (evt) => component.mapPreCompose.emit(evt));
    component.map.on('propertychange', (evt) => component.mapPropertyChange.emit(evt));
    component.map.on('rendercomplete', (evt) => component.mapRenderComplete.emit(evt));
    component.map.on('singleclick', (evt) => component.mapSingleClick.emit(evt));
  }

}
