import { Injectable } from '@angular/core';
import { Tile } from 'ol/layer';
import BaseLayer from 'ol/layer/Base';
import TileImage from 'ol/source/TileImage';
import { MapComponent } from '../m-map.component';
import { MapLayer } from '../m-map.decorator';
import { LayerInputConfig } from '../m-map.model';
import { MapLayerBase } from './abstract.layer';
import { XYZDynamicSource } from '../sources/xyz-dynamic.source';

@Injectable({ providedIn: 'root' })
@MapLayer('core:dynamic')
export class XYZDynamicLayer extends MapLayerBase {

  constructor(public source: XYZDynamicSource) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    return new Tile({
      source: this.source.getSource() as TileImage,
    });
  }

}
