import { Injectable } from '@angular/core';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { MapComponent } from '../m-map.component';
import { MapLayer } from '../m-map.decorator';
import { LayerInputConfig } from '../m-map.model';
import { GeolocationSource } from '../sources/geolocation.source';
import { MapLayerBase } from './abstract.layer';

@Injectable({ providedIn: 'root' })
@MapLayer('core:geolocation')
export class GeolocationLayer extends MapLayerBase {

  constructor(
    public source: GeolocationSource
  ) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
    });
    return layer;
  }

}
