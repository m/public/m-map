import { Injectable } from '@angular/core';
import { Tile } from 'ol/layer';
import BaseLayer from 'ol/layer/Base';
import TileImage from 'ol/source/TileImage';
import { MapComponent } from '../m-map.component';
import { MapLayer } from '../m-map.decorator';
import { LayerInputConfig } from '../m-map.model';
import { XYZDarkSource } from '../sources/xyz-dark.source';
import { MapLayerBase } from './abstract.layer';

@Injectable({ providedIn: 'root' })
@MapLayer('core:xyz-dark')
export class XYZDarkLayer extends MapLayerBase {

  constructor(public source: XYZDarkSource) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    return new Tile({
      source: this.source.getSource() as TileImage,
    });
  }

}
