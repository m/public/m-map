import { getUid } from 'ol';
import Base from 'ol/layer/Base';
import { MapComponent } from '../m-map.component';
import { LayerInputConfig, MapLayerInterface, MapSourceInterface } from '../m-map.model';

export abstract class MapLayerBase implements MapLayerInterface {

  source!: MapSourceInterface;

  abstract buildLayer(component: MapComponent, config: LayerInputConfig): Base;

  show(component: MapComponent, id: string | number): void {
    component.map.getLayers().forEach(layer => {
      if (id === getUid(layer)) {
        layer.setVisible(true);
        const config = [...component.tiles, ...component.layers].find(c => c._uid === id);
        if (config) {
          config.visible = true;
        }
      }
    });
  }

  hide(component: MapComponent, id: string | number): void {
    component.map.getLayers().forEach(layer => {
      if (id === getUid(layer)) {
        layer.setVisible(false);
        const config = [...component.tiles, ...component.layers].find(c => c._uid === id);
        if (config) {
          config.visible = false;
        }
      }
    });
  }

}
