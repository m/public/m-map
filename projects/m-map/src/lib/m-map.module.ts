import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AfterInit } from './behaviors/after-init.map-behavior';
import { AnimatedZoom } from './behaviors/animated-zoom.map-behavior';
import { InitMap } from './behaviors/init.map-behavior';
import { MapEvents } from './behaviors/map-events.map-behavior';
import { GeolocationLayer } from './layers/geolocation.layer';
import { XYZDarkLayer } from './layers/xyz-dark.layer';
import { XYZLightLayer } from './layers/xyz-light.layer';
import { MapComponent } from './m-map.component';
import {
  MAP_BEHAVIOR,
  MAP_LAYER,
  MAP_MAX_EXTENT,
  MAP_PROJECTION_DESTINATION,
  MAP_PROJECTION_SOURCE,
  MAP_XYZ_DARK_URL,
  MAP_XYZ_LIGHT_URL
} from './m-map.token';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { XYZDynamicLayer } from './layers/xyz-dynamic.layer';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [MapComponent],
  providers: [
    // Behaviors
    { provide: MAP_BEHAVIOR, useClass: InitMap, multi: true },
    { provide: MAP_BEHAVIOR, useClass: AfterInit, multi: true },
    { provide: MAP_BEHAVIOR, useClass: MapEvents, multi: true },
    { provide: MAP_BEHAVIOR, useClass: AnimatedZoom, multi: true },
    // Layers
    { provide: MAP_LAYER, useClass: XYZLightLayer, multi: true },
    { provide: MAP_LAYER, useClass: XYZDarkLayer, multi: true },
    { provide: MAP_LAYER, useClass: XYZDynamicLayer, multi: true },
    { provide: MAP_LAYER, useClass: GeolocationLayer, multi: true },
    // Configurations
    { provide: MAP_XYZ_LIGHT_URL, useValue: 'https://data.mobilites-m.fr/carte' },
    { provide: MAP_XYZ_DARK_URL, useValue: 'https://data.mobilites-m.fr/carte-dark' },
    { provide: MAP_MAX_EXTENT, useValue: [5.0200, 44.6800, 6.5300, 45.5600] }, // Gauche, Bas, Droite, Haut
    { provide: MAP_PROJECTION_SOURCE, useValue: 'EPSG:4326' },
    { provide: MAP_PROJECTION_DESTINATION, useValue: 'EPSG:3857' },

  ]
})
export class MMapModule { }
